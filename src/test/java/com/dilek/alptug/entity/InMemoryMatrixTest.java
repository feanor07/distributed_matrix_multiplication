package com.dilek.alptug.entity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static util.TestUtil.createDoubleArray;

/**
 * Test class for {@link InMemoryMatrix}
 *
 * @author alptugd
 */
public class InMemoryMatrixTest {

    private int[][] smallMatrixToUseInPartitioningExceptionTests = new int[3][2];

    @Test
    public void testSubmatrixAdditionThatFitsWithinLimits() throws Exception {
        InMemoryMatrix matrixRepresentation = new InMemoryMatrix(createDoubleArray(30, 20));

        assertEquals(720, matrixRepresentation.getValue(16, 12));
        assertEquals(740, matrixRepresentation.getValue(16, 13));
        assertEquals(760, matrixRepresentation.getValue(16, 14));
        assertEquals(750, matrixRepresentation.getValue(17, 12));
        assertEquals(770, matrixRepresentation.getValue(17, 13));
        assertEquals(790, matrixRepresentation.getValue(17, 14));
        assertEquals(810, matrixRepresentation.getValue(17, 15));
        assertEquals(820, matrixRepresentation.getValue(18, 14));
        assertEquals(840, matrixRepresentation.getValue(18, 15));
        assertEquals(850, matrixRepresentation.getValue(19, 14));
        assertEquals(870, matrixRepresentation.getValue(19, 15));

        int[][] submatrix = createDoubleArray(2, 3);
        matrixRepresentation.add(16, 12, submatrix);

        assertEquals(720, matrixRepresentation.getValue(16, 12));
        assertEquals(743, matrixRepresentation.getValue(16, 13));
        assertEquals(766, matrixRepresentation.getValue(16, 14));
        assertEquals(752, matrixRepresentation.getValue(17, 12));
        assertEquals(775, matrixRepresentation.getValue(17, 13));
        assertEquals(798, matrixRepresentation.getValue(17, 14));
        assertEquals(810, matrixRepresentation.getValue(17, 15));
        assertEquals(820, matrixRepresentation.getValue(18, 14));
        assertEquals(840, matrixRepresentation.getValue(18, 15));
        assertEquals(850, matrixRepresentation.getValue(19, 14));
        assertEquals(870, matrixRepresentation.getValue(19, 15));

        submatrix = createDoubleArray(3, 2);
        matrixRepresentation.add(17, 14, submatrix);

        assertEquals(720, matrixRepresentation.getValue(16, 12));
        assertEquals(743, matrixRepresentation.getValue(16, 13));
        assertEquals(766, matrixRepresentation.getValue(16, 14));
        assertEquals(752, matrixRepresentation.getValue(17, 12));
        assertEquals(775, matrixRepresentation.getValue(17, 13));
        assertEquals(798, matrixRepresentation.getValue(17, 14));
        assertEquals(812, matrixRepresentation.getValue(17, 15));
        assertEquals(823, matrixRepresentation.getValue(18, 14));
        assertEquals(845, matrixRepresentation.getValue(18, 15));
        assertEquals(856, matrixRepresentation.getValue(19, 14));
        assertEquals(878, matrixRepresentation.getValue(19, 15));
    }

    @Test
    public void testSubmatrixAdditionThatExceedsMatrixLimits() throws Exception {
        InMemoryMatrix matrixRepresentation = new InMemoryMatrix(createDoubleArray(30, 20));

        assertEquals(1230, matrixRepresentation.getValue(29, 18));
        assertEquals(1250, matrixRepresentation.getValue(29, 19));

        int[][] submatrix = createDoubleArray(2, 3);
        matrixRepresentation.add(29, 18, submatrix);
        assertEquals(1230, matrixRepresentation.getValue(29, 18));
        assertEquals(1253, matrixRepresentation.getValue(29, 19));
    }


    @Test
    public void testPartitionMatrixSuccess() throws Exception {

        InMemoryMatrix matrix = new InMemoryMatrix(createDoubleArray(749, 856));

        int[][] subMatrix = matrix.partition(0, 0, 83, 85);

        assertEquals(83, subMatrix.length);
        assertEquals(85, subMatrix[0].length);
        assertEquals(0, subMatrix[0][0]);
        assertEquals(38199, subMatrix[3][42]);
        assertEquals(66447, subMatrix[51][33]);
        assertEquals(133322, subMatrix[82][84]);

        subMatrix = matrix.partition(149, 720, 56, 29);
        assertEquals(56, subMatrix.length);
        assertEquals(29, subMatrix[0].length);
        assertEquals( 727921, subMatrix[0][0]);
        assertEquals(749642, subMatrix[13][14]);
        assertEquals(767511, subMatrix[30][20]);
        assertEquals(793084, subMatrix[55][28]);

        subMatrix = matrix.partition(665, 770, 84, 86);
        assertEquals(84, subMatrix.length);
        assertEquals(86, subMatrix[0].length);
        assertEquals( 1157205, subMatrix[0][0]);
        assertEquals( 1213915, subMatrix[30][40]);
        assertEquals( 1273193, subMatrix[60][83]);
        assertEquals(1292132, subMatrix[83][85]);
    }

    @Test(expected = InMemoryMatrix.InvalidMatrixException.class)
    public void testPartitionMatrixThrowsExceptionWithZeroRowLengthDoubleArray() throws Exception {
        new InMemoryMatrix(new int[0][0]);
    }

    @Test(expected = InMemoryMatrix.InvalidMatrixException.class)
    public void testPartitionMatrixThrowsExceptionWithZeroColumnLengthDoubleArray() throws Exception {
        new InMemoryMatrix(new int[3][0]);
    }

    @Test(expected = InMemoryMatrix.InvalidMatrixException.class)
    public void testPartitionMatrixThrowsExceptionWithVaryingColumnLengthDoubleArray() throws Exception {
        int[][] varyingLengthDoubleArray = new int[3][];
        varyingLengthDoubleArray[0] = new int[2];
        varyingLengthDoubleArray[1] = new int[2];
        varyingLengthDoubleArray[2] = new int[3];

        new InMemoryMatrix(varyingLengthDoubleArray);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPartitionMatrixThrowsExceptionWithZeroRowSubMatrixRequest() throws Exception {
        new InMemoryMatrix(smallMatrixToUseInPartitioningExceptionTests).partition(0,0,0,1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPartitionMatrixThrowsExceptionWithZeroColumnSubMatrixRequest() throws Exception {
        new InMemoryMatrix(smallMatrixToUseInPartitioningExceptionTests).partition(0,0,1,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPartitionMatrixThrowsExceptionWithNegativeRowIndexSubMatrixRequest() throws Exception {
        new InMemoryMatrix(smallMatrixToUseInPartitioningExceptionTests).partition(-1,0,1,1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPartitionMatrixThrowsExceptionWithNegativeColumnIndexSubMatrixRequest() throws Exception {
        new InMemoryMatrix(smallMatrixToUseInPartitioningExceptionTests).partition(1,-1,1,1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPartitionMatrixThrowsExceptionWithInvalidRowPartitioningSubMatrixRequest() throws Exception {
        new InMemoryMatrix(smallMatrixToUseInPartitioningExceptionTests).partition(1,0,4,1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPartitionMatrixThrowsExceptionWithInvalidColumnPartitioningSubMatrixRequest() throws Exception {
        new InMemoryMatrix(smallMatrixToUseInPartitioningExceptionTests).partition(1,1,1,2);
    }
}