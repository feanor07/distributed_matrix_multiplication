package com.dilek.alptug.datareceiver;

import com.dilek.alptug.datasender.UDPBasedObjectSender;
import com.dilek.alptug.entity.TaskResult;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link UDPBasedObjectReceiver}
 *
 * @author alptugd
 */
public class UDPBasedObjectReceiverTest {
    private static final int PORT = 9999;

    private UDPBasedObjectReceiver<TaskResult> receiver = new UDPBasedObjectReceiver<>();

    @Test
    public void testReceiveObject() throws Exception {
        TaskResult resultToSend = createTaskToSend();
        final List<TaskResult> resultArray = new ArrayList<>();

        new Thread(()-> receiver.startReceiving(PORT, (taskResult)-> resultArray.add(taskResult))).start();

        UDPBasedObjectSender<TaskResult> sender = new UDPBasedObjectSender<>();
        sender.sendObject(resultToSend, new InetSocketAddress("localhost", PORT));
        Thread.sleep(2000);

        TaskResult received = resultArray.get(0);

        assertEquals(1, received.getTaskId());
        assertEquals(0, received.getResult()[0][0]);
        assertEquals(1, received.getResult()[0][1]);
        assertEquals(2, received.getResult()[1][0]);
        assertEquals(3, received.getResult()[1][1]);
        assertEquals(4, received.getResult()[2][0]);
        assertEquals(5, received.getResult()[2][1]);
        assertEquals(3, received.getResult().length);
    }

    private TaskResult createTaskToSend() {
        TaskResult resultToSend = new TaskResult();
        resultToSend.setTaskId(1);
        resultToSend.setResult(new int[][]{{0,1},{2,3},{4,5}});

        return resultToSend;
    }
}