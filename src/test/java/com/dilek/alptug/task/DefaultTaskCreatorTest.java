package com.dilek.alptug.task;

import com.dilek.alptug.entity.DefaultTaskDefinitionBuilder;
import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskDefinition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static util.TestUtil.createDoubleArray;

/**
 * Test class for {@link TaskCreator}
 *
 * @author alptugd
 */
public class DefaultTaskCreatorTest {

    private static final int RESULT_COLUMN_INDEX = 2;
    private static final int RESULT_ROW_INDEX = 6;
    private static final int VECTOR_LENGTH = 5;
    private static final int ROW_COUNT = 3;
    private static final int ID = 1;
    private static final int COLUMN_COUNT = 2;
    private static final int COMMON_INDEX = 15;
    private static final int REPLY_PORT = 1512;
    private static final String REPLY_ADDRESS = "255.255.255.255";

    private DefaultTaskCreator taskCreator = new DefaultTaskCreator(REPLY_ADDRESS, REPLY_PORT);

    @Mock
    private Matrix first;

    @Mock
    private Matrix second;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void after() {
        verifyNoMoreInteractions(first, second);
    }

    @Test
    public void createTask() throws Exception {
        TaskDefinition taskDefinition = new DefaultTaskDefinitionBuilder().setResultColumnIndex(RESULT_COLUMN_INDEX).setResultRowIndex(
                RESULT_ROW_INDEX).setVectorLength(VECTOR_LENGTH).setRowCount(ROW_COUNT).setColumnCount(
                        COLUMN_COUNT).setId(ID).setCommonIndex(COMMON_INDEX).build();

        when(first.partition(RESULT_ROW_INDEX, COMMON_INDEX, ROW_COUNT, VECTOR_LENGTH)).thenReturn(createDoubleArray(ROW_COUNT, VECTOR_LENGTH));
        when(second.partition(COMMON_INDEX, RESULT_COLUMN_INDEX, VECTOR_LENGTH, COLUMN_COUNT)).thenReturn(createDoubleArray(VECTOR_LENGTH, COLUMN_COUNT));

        Task task = taskCreator.createTask(taskDefinition, first, second);

        int[] rows = task.getRowVectors();
        int[] columns = task.getColumnVectors();

        assertEquals(VECTOR_LENGTH, task.getVectorLength());
        assertEquals(ID, task.getTaskId());
        assertRows(rows);
        assertColumns(columns);
        assertEquals(REPLY_ADDRESS, task.getReplyAddress());
        assertEquals(REPLY_PORT, task.getReplyPort());

        verify(first).partition(RESULT_ROW_INDEX, COMMON_INDEX, ROW_COUNT, VECTOR_LENGTH);
        verify(second).partition(COMMON_INDEX, RESULT_COLUMN_INDEX, VECTOR_LENGTH, COLUMN_COUNT);
    }

    private void assertRows(int[] rows) {
        assertEquals(ROW_COUNT * VECTOR_LENGTH, rows.length);
        assertEquals(0, rows[0]);
        assertEquals(5, rows[1]);
        assertEquals(10, rows[2]);
        assertEquals(15, rows[3]);
        assertEquals(20, rows[4]);
        assertEquals(3, rows[5]);
        assertEquals(8, rows[6]);
        assertEquals(13, rows[7]);
        assertEquals(18, rows[8]);
        assertEquals(23, rows[9]);
        assertEquals(6, rows[10]);
        assertEquals(11, rows[11]);
        assertEquals(16, rows[12]);
        assertEquals(21, rows[13]);
        assertEquals(26, rows[14]);
    }

    private void assertColumns(int[] columns) {
        assertEquals(COLUMN_COUNT * VECTOR_LENGTH, columns.length);
        assertEquals(0, columns[0]);
        assertEquals(5, columns[1]);
        assertEquals(10, columns[2]);
        assertEquals(15, columns[3]);
        assertEquals(20, columns[4]);
        assertEquals(2, columns[5]);
        assertEquals(7, columns[6]);
        assertEquals(12, columns[7]);
        assertEquals(17, columns[8]);
        assertEquals(22, columns[9]);
    }

}