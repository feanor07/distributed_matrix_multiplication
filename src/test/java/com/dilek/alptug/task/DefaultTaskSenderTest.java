package com.dilek.alptug.task;

import com.dilek.alptug.datasender.ObjectSender;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.Worker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.InetSocketAddress;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultTaskSender}
 *
 * @author alptugd
 */
public class DefaultTaskSenderTest {

    @Mock
    private ObjectSender<Task> objectSender;

    private DefaultTaskSender taskSender;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        taskSender = new DefaultTaskSender(objectSender);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(objectSender);
    }

    @Test
    public void testSendTask() {
        Worker worker =  new Worker();
        worker.setIp("localhost");
        worker.setPort(9876);

        Task task = new Task();
        InetSocketAddress address = new InetSocketAddress("localhost", 9876);

        when(objectSender.sendObject(task, address)).thenReturn(true);
        assertTrue(taskSender.sendTask(task, worker));


        verify(objectSender).sendObject(task, address);
    }
}