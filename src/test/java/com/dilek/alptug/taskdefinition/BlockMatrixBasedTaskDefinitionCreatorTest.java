package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Iterator;

/**
 * Test class for {@link BlockMatrixBasedTaskDefinitionCreator}
 *
 * @author alptugd
 */
public class BlockMatrixBasedTaskDefinitionCreatorTest {

    @Test
    public void testTaskDefinitionCreationFor749x856x291() throws Exception {
        BlockMatrixBasedTaskDefinitionCreator taskPartitionCreator = new BlockMatrixBasedTaskDefinitionCreator(
                new DefaultTaskDefinitionBuilderFactory(), 749, 856, 291,90);

        Iterator<TaskDefinition> iter = taskPartitionCreator.requestTaskDefinitions(11).iterator();
        assertTaskDefinition(iter.next(), 0, 83, 0,
                72, 0, 85, 1);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 85, 85, 2);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 170, 85, 3);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 255, 85, 4);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 340, 86, 5);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 426, 86, 6);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 512, 86, 7);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 598, 86, 8);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 684, 86, 9);
        assertTaskDefinition(iter.next(), 0, 83,0,
                72, 770, 86, 10);
        assertTaskDefinition(iter.next(), 0, 83,72,
                73, 0, 85, 11);
        taskPartitionCreator.requestTaskDefinitions(17);
        iter = taskPartitionCreator.requestTaskDefinitions(11).iterator();
        assertTaskDefinition(iter.next(), 0, 83,145,
                73, 684, 86, 29);
        iter = taskPartitionCreator.requestTaskDefinitions(124).iterator();
        assertTaskDefinition(iter.next(), 0, 83,218,
                73, 770, 86, 40);
        assertTaskDefinition(iter.next(), 83, 83,0,
                72, 0, 85, 41);
        iter = taskPartitionCreator.requestTaskDefinitions(116).iterator();
        assertTaskDefinition(iter.next(), 332, 83,0,
                72, 255, 85, 164);
        iter = taskPartitionCreator.requestTaskDefinitions(70).iterator();
        assertTaskDefinition(iter.next(), 498, 83,218,
                73, 770, 86, 280);
        assertTaskDefinition(iter.next(), 581, 84,0,
                72, 0, 85, 281);
        assertFalse(taskPartitionCreator.isDonePartitioning());
        iter = taskPartitionCreator.requestTaskDefinitions(100).iterator();
        assertTrue(taskPartitionCreator.isDonePartitioning());
        assertTaskDefinition(iter.next(), 665, 84,145,
                73, 770, 86, 350);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 0, 85, 351);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 85, 85, 352);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 170, 85, 353);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 255, 85, 354);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 340, 86, 355);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 426, 86, 356);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 512, 86, 357);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 598, 86, 358);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 684, 86, 359);
        assertTaskDefinition(iter.next(), 665, 84,218,
                73, 770, 86, 360);
        assertFalse(iter.hasNext());
        iter = taskPartitionCreator.requestTaskDefinitions(1).iterator();
        assertFalse(iter.hasNext());
    }

    @Test
    public void testTaskDefinitionCreationFor1000x1000x1000() throws Exception {
        BlockMatrixBasedTaskDefinitionCreator taskPartitionCreator = new BlockMatrixBasedTaskDefinitionCreator(
                new DefaultTaskDefinitionBuilderFactory(), 1000, 1000, 1000, 90);
        Iterator<TaskDefinition> iter = taskPartitionCreator.requestTaskDefinitions(13).iterator();
        assertTaskDefinition(iter.next(), 0, 83, 0,
                83, 0, 83, 10001);
        assertTaskDefinition(iter.next(), 0, 83,0,
                83, 83, 83, 10002);
        iter = taskPartitionCreator.requestTaskDefinitions(1712).iterator();
        assertTaskDefinition(iter.next(), 0, 83,83,
                83, 83, 83, 10014);
        iter = taskPartitionCreator.requestTaskDefinitions(100).iterator();
        assertTaskDefinition(iter.next(), 916, 84,916,
                84, 748, 84, 11726);
        assertTaskDefinition(iter.next(), 916, 84,916,
                84, 832, 84, 11727);
        assertTaskDefinition(iter.next(), 916, 84,916,
                84, 916, 84, 11728);
        assertFalse(iter.hasNext());
    }

    private void assertTaskDefinition(TaskDefinition taskDefinition, int expectedResultRowIndex, int expectedRowCount,
                                      int expectedResultColumnIndex, int expectedColumnCount, int expectedCommonIndex,
                                      int expectedVectorLength, int expectedId) {
        assertEquals(expectedResultRowIndex, taskDefinition.getResultRowIndex());
        assertEquals(expectedRowCount, taskDefinition.getRowCount());
        assertEquals(expectedResultColumnIndex, taskDefinition.getResultColumnIndex());
        assertEquals(expectedColumnCount, taskDefinition.getColumnCount());
        assertEquals(expectedCommonIndex, taskDefinition.getCommonIndex());
        assertEquals(expectedVectorLength, taskDefinition.getVectorLength());

        if (taskDefinition.getId() > 10000 && expectedId < 10000) {
            assertEquals(expectedId+10000, taskDefinition.getId());
        } else if (taskDefinition.getId() < 10000 && expectedId > 10000) {
            assertEquals(expectedId, taskDefinition.getId()+10000);
        } else {
            assertEquals(expectedId, taskDefinition.getId());
        }
    }
}