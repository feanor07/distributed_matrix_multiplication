package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.DefaultTaskDefinitionBuilder;
import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.TaskDefinitionBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Test class for {@link InMemoryTaskDefinitionRepository}
 *
 * @author alptugd
 */
public class InMemoryTaskDefinitionRepositoryTest {

    private InMemoryTaskDefinitionRepository taskDefinitionRepository = new InMemoryTaskDefinitionRepository();

    private TaskDefinitionBuilder builder = new DefaultTaskDefinitionBuilder();

    private TaskDefinition first;

    private TaskDefinition second;

    private TaskDefinition third;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        first = builder.setId(1).build();
        second = builder.setId(2).build();
        third = builder.setId(3).build();
    }

    @Test
    public void testOperations() throws Exception {
        taskDefinitionRepository.persist(Arrays.asList(first, second, third));

        assertFalse(taskDefinitionRepository.areAllTaskDefinitionsFinished());
        assertSame(first, taskDefinitionRepository.getTaskDefinition(1));

        taskDefinitionRepository.markTaskDefinitionAsFinished(2);
        taskDefinitionRepository.markTaskDefinitionAsFinished(3);
        assertFalse(taskDefinitionRepository.areAllTaskDefinitionsFinished());

        taskDefinitionRepository.markTaskDefinitionAsFinished(1);
        assertTrue(taskDefinitionRepository.areAllTaskDefinitionsFinished());
    }

}