package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.worker.WorkerBuffer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link RemainingTaskDefinitionProvider}
 *
 * @author alptugd
 */
public class RemainingTaskDefinitionProviderTest {
    private RemainingTaskDefinitionProvider remainingTaskDefinitionProvider;

    @Mock
    private TaskDefinitionRepository taskDefinitionRepository;

    @Mock
    private TaskDefinition firstTaskDefinition;

    @Mock
    private TaskDefinition secondTaskDefinition;

    @Mock
    private TaskDefinition thirdTaskDefinition;

    @Mock
    private WorkerBuffer firstWorkerBuffer;

    @Mock
    private WorkerBuffer secondWorkerBuffer;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        remainingTaskDefinitionProvider = new RemainingTaskDefinitionProvider(taskDefinitionRepository, Arrays.asList(firstWorkerBuffer, secondWorkerBuffer));
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(firstTaskDefinition, secondTaskDefinition, thirdTaskDefinition, firstWorkerBuffer, secondWorkerBuffer);
    }

    @Test
    public void testIsDonePartitioning() throws Exception {
        when(taskDefinitionRepository.areAllTaskDefinitionsFinished()).thenReturn(false, true);

        assertFalse(remainingTaskDefinitionProvider.isDonePartitioning());
        assertTrue(remainingTaskDefinitionProvider.isDonePartitioning());

        verify(taskDefinitionRepository, times(2)).areAllTaskDefinitionsFinished();
    }

    @Test
    public void testRequestTaskDefinitions() {
        when(firstWorkerBuffer.isSuspended()).thenReturn(false, true);
        when(secondWorkerBuffer.isSuspended()).thenReturn(false, false, true);
        when(firstWorkerBuffer.getAllTaskDefinitions()).thenReturn(Arrays.asList(firstTaskDefinition, secondTaskDefinition), Collections.emptyList());
        when(secondWorkerBuffer.getAllTaskDefinitions()).thenReturn(Collections.singleton(thirdTaskDefinition));

        Iterator<TaskDefinition> iterator = remainingTaskDefinitionProvider.requestTaskDefinitions(10).iterator();
        assertFalse(iterator.hasNext());
        verify(firstWorkerBuffer).isSuspended();
        verify(secondWorkerBuffer).isSuspended();

        iterator = remainingTaskDefinitionProvider.requestTaskDefinitions(10).iterator();
        assertSame(firstTaskDefinition, iterator.next());
        verify(firstWorkerBuffer).getAllTaskDefinitions();
        verify(firstWorkerBuffer, times(2)).isSuspended();
        verify(secondWorkerBuffer, times(2)).isSuspended();
        assertFalse(iterator.hasNext());

        iterator = remainingTaskDefinitionProvider.requestTaskDefinitions(10).iterator();
        assertSame(secondTaskDefinition, iterator.next());
        assertFalse(iterator.hasNext());

        iterator = remainingTaskDefinitionProvider.requestTaskDefinitions(10).iterator();
        assertSame(thirdTaskDefinition, iterator.next());
        assertFalse(iterator.hasNext());
        verify(firstWorkerBuffer, times(3)).isSuspended();
        verify(secondWorkerBuffer, times(3)).isSuspended();
        verify(firstWorkerBuffer, times(2)).getAllTaskDefinitions();
        verify(secondWorkerBuffer).getAllTaskDefinitions();
    }
}