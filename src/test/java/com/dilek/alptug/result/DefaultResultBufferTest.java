package com.dilek.alptug.result;

import com.dilek.alptug.entity.TaskResult;
import org.junit.Test;

import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Test class for {@link DefaultResultBuffer}
 *
 * @author alptugd
 */
public class DefaultResultBufferTest {
    private DefaultResultBuffer resultBuffer = new DefaultResultBuffer();

    @Test
    public void testResultBuffer() {
        assertFalse(resultBuffer.isDone());

        TaskResult firstResult = new TaskResult();
        TaskResult secondResult = new TaskResult();
        TaskResult thirdResult = new TaskResult();
        TaskResult fourthResult = new TaskResult();
        TaskResult fifthResult = new TaskResult();

        assertEquals(0, resultBuffer.retrieveTaskResults().size());

        resultBuffer.putTaskResult(firstResult);
        resultBuffer.putTaskResult(secondResult);

        Collection<TaskResult> results = resultBuffer.retrieveTaskResults();
        assertEquals(2, results.size());

        Iterator<TaskResult> iter = results.iterator();
        assertSame(firstResult, iter.next());
        assertSame(secondResult, iter.next());

        resultBuffer.putTaskResult(thirdResult);
        resultBuffer.putTaskResult(fourthResult);
        resultBuffer.putTaskResult(fifthResult);
        resultBuffer.setFinished();

        assertFalse(resultBuffer.isDone());
        results = resultBuffer.retrieveTaskResults();
        assertEquals(3, results.size());
        assertTrue(resultBuffer.isDone());
        iter = results.iterator();
        assertSame(thirdResult, iter.next());
        assertSame(fourthResult, iter.next());
        assertSame(fifthResult, iter.next());
    }

    @Test(expected = IllegalStateException.class)
    public void testPuttingToADoneBuffer() {
        assertFalse(resultBuffer.isDone());
        resultBuffer.setFinished();
        resultBuffer.putTaskResult(new TaskResult());
    }
}