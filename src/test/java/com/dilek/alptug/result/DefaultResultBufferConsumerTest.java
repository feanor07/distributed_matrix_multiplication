package com.dilek.alptug.result;

import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.TaskResult;
import com.dilek.alptug.entity.Worker;
import com.dilek.alptug.taskdefinition.TaskDefinitionRepository;
import com.dilek.alptug.worker.WorkerBuffer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultResultBufferConsumer}
 *
 * @author alptugd
 */
public class DefaultResultBufferConsumerTest {

    private DefaultResultBufferConsumer resultBufferConsumer;

    @Mock
    private TaskDefinitionRepository taskDefinitionRepository;

    @Mock
    private Matrix resultMatrix;

    private int consumeCycleInMilliseconds = 100;

    @Mock
    private ResultBuffer resultBuffer;

    @Mock
    private TaskDefinition firstTaskDefinition;

    @Mock
    private TaskDefinition secondTaskDefinition;

    @Mock
    private TaskResult firstTaskResult;

    @Mock
    private TaskResult secondTaskResult;

    @Mock
    private TaskResult thirdTaskResult;

    @Mock
    private WorkerBuffer firstWorkerBuffer;

    @Mock
    private Worker firstWorker;

    @Mock
    private Worker secondWorker;

    @Mock
    private WorkerBuffer secondWorkerBuffer;

    private int[][] firstSubMatrix = new int[20][20];

    private int[][] secondSubMatrix = new int[10][10];

    private String firstWorkerIp = "1.1.1.1";
    private String secondWorkerIp = "2.1.1.1";

    private int firstWorkerPort = 1;
    private int secondWorkerPort = 2;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        resultBufferConsumer = new DefaultResultBufferConsumer(taskDefinitionRepository,
                Arrays.asList(firstWorkerBuffer, secondWorkerBuffer), resultMatrix, consumeCycleInMilliseconds);
    }

    @Test
    public void testConsume() throws Exception {
        when(resultBuffer.isDone()).thenReturn(false, true);
        when(resultBuffer.retrieveTaskResults()).thenReturn(Arrays.asList(firstTaskResult, secondTaskResult, thirdTaskResult));

        when(firstTaskResult.getTaskId()).thenReturn(1);
        when(firstTaskResult.getReceivedFromIp()).thenReturn(firstWorkerIp);
        when(firstTaskResult.getReceivedFromPort()).thenReturn(firstWorkerPort);
        when(secondTaskResult.getTaskId()).thenReturn(2);
        when(secondTaskResult.getReceivedFromIp()).thenReturn(secondWorkerIp);
        when(secondTaskResult.getReceivedFromPort()).thenReturn(secondWorkerPort);
        when(thirdTaskResult.getTaskId()).thenReturn(3);
        when(thirdTaskResult.getReceivedFromIp()).thenReturn(firstWorkerIp);
        when(thirdTaskResult.getReceivedFromPort()).thenReturn(firstWorkerPort);
        when(firstTaskResult.getResult()).thenReturn(firstSubMatrix);
        when(secondTaskResult.getResult()).thenReturn(secondSubMatrix);

        when(firstWorkerBuffer.getWorker()).thenReturn(firstWorker);
        when(secondWorkerBuffer.getWorker()).thenReturn(secondWorker);
        when(firstWorker.getIp()).thenReturn(firstWorkerIp);
        when(firstWorker.getPort()).thenReturn(firstWorkerPort);
        when(secondWorker.getIp()).thenReturn(secondWorkerIp);
        when(secondWorker.getPort()).thenReturn(secondWorkerPort);

        when(taskDefinitionRepository.markTaskDefinitionAsFinished(1)).thenReturn(true);
        when(taskDefinitionRepository.markTaskDefinitionAsFinished(2)).thenReturn(true);
        when(taskDefinitionRepository.markTaskDefinitionAsFinished(3)).thenReturn(false);

        when(taskDefinitionRepository.getTaskDefinition(1)).thenReturn(firstTaskDefinition);
        when(taskDefinitionRepository.getTaskDefinition(2)).thenReturn(secondTaskDefinition);

        when(firstTaskDefinition.getResultRowIndex()).thenReturn(0);
        when(secondTaskDefinition.getResultRowIndex()).thenReturn(10);
        when(firstTaskDefinition.getResultColumnIndex()).thenReturn(20);
        when(secondTaskDefinition.getResultColumnIndex()).thenReturn(50);

        resultBufferConsumer.accept(resultBuffer);

        Thread.sleep(1000);

        verify(resultBuffer, times(2)).isDone();
        verify(resultBuffer).retrieveTaskResults();
        verify(firstTaskResult).getTaskId();
        verify(secondTaskResult).getTaskId();
        verify(thirdTaskResult).getTaskId();
        verify(firstTaskResult).getReceivedFromPort();
        verify(secondTaskResult).getReceivedFromPort();
        verify(thirdTaskResult).getReceivedFromPort();
        verify(firstTaskResult).getReceivedFromIp();
        verify(secondTaskResult).getReceivedFromIp();
        verify(thirdTaskResult).getReceivedFromIp();

        verify(taskDefinitionRepository).markTaskDefinitionAsFinished(1);
        verify(taskDefinitionRepository).markTaskDefinitionAsFinished(2);
        verify(taskDefinitionRepository).markTaskDefinitionAsFinished(3);

        verify(taskDefinitionRepository).getTaskDefinition(1);
        verify(taskDefinitionRepository).getTaskDefinition(2);

        verify(firstTaskDefinition).getResultRowIndex();
        verify(firstTaskDefinition).getResultColumnIndex();
        verify(secondTaskDefinition).getResultRowIndex();
        verify(secondTaskDefinition).getResultColumnIndex();

        verify(resultMatrix).add(0, 20, firstSubMatrix);
        verify(resultMatrix).add(10, 50, secondSubMatrix);

        verify(firstTaskResult).getResult();
        verify(secondTaskResult).getResult();

        verify(firstWorkerBuffer, times(3)).getWorker();
        verify(secondWorkerBuffer, times(1)).getWorker();
        verify(firstWorker, times(3)).getIp();
        verify(firstWorker, times(2)).getPort();
        verify(secondWorker, times(1)).getIp();
        verify(secondWorker, times(1)).getPort();

        verify(firstWorkerBuffer, times(2)).setLastResponseTime(anyLong());
        verify(secondWorkerBuffer, times(1)).setLastResponseTime(anyLong());

        verifyZeroInteractions(resultMatrix);
        verifyNoMoreInteractions(firstTaskDefinition, secondTaskDefinition, firstTaskResult, secondTaskResult,
                thirdTaskResult, resultBuffer, taskDefinitionRepository, firstWorkerBuffer, secondWorkerBuffer,
                firstWorker, secondWorker);
    }
}