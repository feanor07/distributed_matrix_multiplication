package com.dilek.alptug.result;

import com.dilek.alptug.datareceiver.ObjectReceiver;
import com.dilek.alptug.entity.TaskResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.SocketTimeoutException;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultResultReceiver}
 *
 * @author alptugd
 */
public class DefaultResultReceiverTest {
    private static final int PORT = 123;
    private DefaultResultReceiver defaultResultReceiver;

    @Mock
    private ObjectReceiver<TaskResult> objectReceiver;

    @Mock
    private Consumer<TaskResult> consumer;

    @Before
    public  void test() {
        MockitoAnnotations.initMocks(this);
        defaultResultReceiver = new DefaultResultReceiver(objectReceiver, consumer);
    }

    @Test
    public void testReceiving() throws Exception {
        doThrow(new RuntimeException(new SocketTimeoutException())).doNothing().when(objectReceiver).startReceiving(PORT, consumer);
        defaultResultReceiver.receiveTaskResult(PORT);
        Thread.sleep(1000);
        verify(objectReceiver, times(2)).startReceiving(PORT, consumer);
        verifyNoMoreInteractions(objectReceiver);
        verifyZeroInteractions(consumer);
    }
}