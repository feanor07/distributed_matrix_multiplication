package com.dilek.alptug.result;

import com.dilek.alptug.entity.TaskResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultResultBufferProducer}
 *
 * @author alptugd
 */
public class DefaultResultBufferProducerTest {

    private DefaultResultBufferProducer producer;

    @Mock
    private TaskResult taskResult;

    @Mock
    private ResultBuffer resultBuffer;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        producer = new DefaultResultBufferProducer(resultBuffer);
    }

    @Test
    public void testBufferProducer() {
        producer.accept(taskResult);
        verify(resultBuffer).putTaskResult(taskResult);
        verify(taskResult).getTaskId();
        verifyNoMoreInteractions(resultBuffer, taskResult);
    }

}