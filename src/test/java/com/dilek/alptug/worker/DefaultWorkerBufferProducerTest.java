package com.dilek.alptug.worker;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.taskdefinition.TaskDefinitionProvider;
import com.dilek.alptug.taskdefinition.TaskDefinitionRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultWorkerBufferProducer}
 *
 * @author alptugd
 */
public class DefaultWorkerBufferProducerTest {

    private DefaultWorkerBufferProducer workerBufferProducer;

    @Mock
    private TaskDefinitionRepository taskDefinitionRepository;

    @Mock
    private TaskDefinitionProvider taskDefinitionProvider;

    @Mock
    private WorkerBuffer workerBuffer;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        workerBufferProducer = new DefaultWorkerBufferProducer(taskDefinitionProvider, taskDefinitionRepository);
    }

    @Test
    public void testProduce() throws Exception {
        List<TaskDefinition> taskDefinitions = Collections.emptyList();

        when(workerBuffer.getFreeSpace()).thenReturn(3, 0, 2);
        when(taskDefinitionProvider.isDonePartitioning()).thenReturn(false, false, true);
        when(taskDefinitionProvider.requestTaskDefinitions(3)).thenReturn(taskDefinitions);

        workerBufferProducer.produce(workerBuffer);
        verify(taskDefinitionProvider).requestTaskDefinitions(3);
        verify(workerBuffer).putTaskDefinitions(taskDefinitions);
        verify(taskDefinitionRepository).persist(taskDefinitions);

        workerBufferProducer.produce(workerBuffer);

        try {
            workerBufferProducer.produce(workerBuffer);
        } catch (WorkerBufferProducer.CannotProduceFurtherException exception) {
        }

        verify(workerBuffer, times(2)).getFreeSpace();
        verify(taskDefinitionProvider, times(3)).isDonePartitioning();

        verifyNoMoreInteractions(workerBuffer, taskDefinitionProvider, taskDefinitionRepository);
    }
}