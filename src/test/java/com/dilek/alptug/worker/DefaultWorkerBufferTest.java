package com.dilek.alptug.worker;

import com.dilek.alptug.entity.DefaultTaskDefinitionBuilder;
import com.dilek.alptug.entity.TaskDefinition;
import org.junit.Test;

import java.util.*;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link DefaultWorkerBuffer}
 *
 * @author alptugd
 */
public class DefaultWorkerBufferTest {
    private static final int CAPACITY = 10;

    private DefaultWorkerBuffer workerBuffer = new DefaultWorkerBuffer(CAPACITY, null);

    @Test
    public void testWorkerBuffer() {
        DefaultTaskDefinitionBuilder taskDefinitionBuilder = new DefaultTaskDefinitionBuilder();

        assertEquals(CAPACITY, workerBuffer.getFreeSpace());
        assertFalse(workerBuffer.isSuspended());
        assertTrue(workerBuffer.getLastResponseTime() + 2000 > System.currentTimeMillis());

        int bufferedWorkload = workerBuffer.putTaskDefinitions(createWorkLoad(1, 7, taskDefinitionBuilder));
        assertTrue(workerBuffer.getLastResponseTime() + 1000 > System.currentTimeMillis());
        assertEquals(7, bufferedWorkload);
        assertEquals(3, workerBuffer.getFreeSpace());

        assertTrue(workerBuffer.getTaskDefinitionOffloadTimes().isEmpty());

        Optional<TaskDefinition> taskDefinition = workerBuffer.requestTaskDefinition();
        assertTrue(taskDefinition.isPresent());
        assertEquals(1, taskDefinition.get().getId());
        taskDefinition.get().markFinished();

        Iterator<Map.Entry<TaskDefinition, Long>> iterator = workerBuffer.getTaskDefinitionOffloadTimes().entrySet().iterator();
        Map.Entry<TaskDefinition, Long> next = iterator.next();
        assertEquals(1, next.getKey().getId());
        assertTrue(next.getValue() + 1000 > System.currentTimeMillis());

        bufferedWorkload = workerBuffer.putTaskDefinitions(createWorkLoad(8, 15, taskDefinitionBuilder));
        assertEquals(3, bufferedWorkload);
        assertEquals(0, workerBuffer.getFreeSpace());

        workerBuffer.removeFinishedTaskDefinitions();
        assertTrue(workerBuffer.getTaskDefinitionOffloadTimes().isEmpty());

        IntStream.range(0, 3).forEach((idx)-> {
            TaskDefinition taskDef = workerBuffer.requestTaskDefinition().get();
            taskDef.markFinished();
        });

        workerBuffer.removeFinishedTaskDefinitions();
        assertEquals(4, workerBuffer.getFreeSpace());

        workerBuffer.suspend();
        assertEquals(0, workerBuffer.getFreeSpace());

        bufferedWorkload = workerBuffer.putTaskDefinitions(createWorkLoad(11, 20, taskDefinitionBuilder));
        assertEquals(0, bufferedWorkload);

        workerBuffer.unsuspend();
        assertEquals(4, workerBuffer.getFreeSpace());
        bufferedWorkload = workerBuffer.putTaskDefinitions(createWorkLoad(11, 20, taskDefinitionBuilder));
        assertEquals(4, bufferedWorkload);
        assertEquals(0, workerBuffer.getFreeSpace());
    }

    private List<TaskDefinition> createWorkLoad(int startIndex, int endIndex, DefaultTaskDefinitionBuilder taskDefinitionBuilder) {
        List<TaskDefinition> workLoad = new ArrayList<>();

        IntStream.rangeClosed(startIndex, endIndex).forEach(index->{
            taskDefinitionBuilder.setId(index);
            workLoad.add(taskDefinitionBuilder.build());
        });

        return workLoad;
    }
}