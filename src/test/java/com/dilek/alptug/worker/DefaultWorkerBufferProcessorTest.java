package com.dilek.alptug.worker;

import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.Worker;
import com.dilek.alptug.task.TaskCreator;
import com.dilek.alptug.task.TaskSender;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultWorkerBufferProcessor}
 *
 * @author alptugd
 */
public class DefaultWorkerBufferProcessorTest {
    @Mock
    private TaskCreator taskCreator;

    @Mock
    private TaskSender taskSender;

    @Mock
    private Matrix firstMatrix;

    @Mock
    private Matrix secondMatrix;

    @Mock
    private TaskDefinition firstTaskDefinition;

    @Mock
    private TaskDefinition secondTaskDefinition;

    @Mock
    private Task firstTask;

    @Mock
    private Task secondTask;

    @Mock
    private WorkerBuffer workerBuffer;

    @Mock
    private Worker worker;

    @Mock
    private WorkerBufferHealthChecker healthChecker;

    private DefaultWorkerBufferProcessor workerBufferProcessor;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        workerBufferProcessor = new DefaultWorkerBufferProcessor(taskCreator, taskSender, healthChecker, firstMatrix, secondMatrix);
    }

    @Test
    public void testConsume() throws Exception {
        when(workerBuffer.requestTaskDefinition()).thenReturn(Optional.of(firstTaskDefinition)).thenReturn(Optional.of(secondTaskDefinition)).thenReturn(Optional.empty());
        when(workerBuffer.getWorker()).thenReturn(worker);

        when(taskCreator.createTask(firstTaskDefinition, firstMatrix, secondMatrix)).thenReturn(firstTask);
        when(taskCreator.createTask(secondTaskDefinition, firstMatrix, secondMatrix)).thenReturn(secondTask);
        when(taskSender.sendTask(firstTask, worker)).thenReturn(true);
        when(taskSender.sendTask(secondTask, worker)).thenReturn(false);

        workerBufferProcessor.process(workerBuffer);

        // Wait for separate threads to satisfy desired interactions.
        Thread.sleep(500);

        verify(workerBuffer, times(3)).requestTaskDefinition();
        verify(workerBuffer).getWorker();
        verify(workerBuffer).removeFinishedTaskDefinitions();
        verify(healthChecker).checkWorkBuffer(workerBuffer);
        verify(taskCreator).createTask(firstTaskDefinition, firstMatrix, secondMatrix);
        verify(taskCreator).createTask(secondTaskDefinition, firstMatrix, secondMatrix);
        verify(taskSender).sendTask(firstTask, worker);
        verify(taskSender).sendTask(secondTask, worker);

        verifyZeroInteractions(firstMatrix, secondMatrix, firstTask, secondTask);
        verifyNoMoreInteractions(workerBuffer, taskCreator, taskSender, firstTaskDefinition, secondTaskDefinition, healthChecker);
    }
}