package com.dilek.alptug.worker;

import com.dilek.alptug.entity.TaskDefinition;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultWorkerBufferHealthChecker}
 *
 * @author alptugd
 */
public class DefaultWorkerBufferHealthCheckerTest {
    private DefaultWorkerBufferHealthChecker healthChecker;

    @Mock
    private WorkerBuffer workerBuffer;

    @Mock
    private TaskDefinition firstTaskDefinition;

    @Mock
    private TaskDefinition secondTaskDefinition;

    private int taskDefinitionSendTimeoutInMilliseconds = 100;

    private int bufferSuspendTimeoutInMilliseconds = 1000;

    private long firstTaskDefinitionOffloadTime = System.currentTimeMillis() + 100000;

    private long secondTaskDefinitionOffloadTime = System.currentTimeMillis() - 101;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        healthChecker = new DefaultWorkerBufferHealthChecker(taskDefinitionSendTimeoutInMilliseconds, bufferSuspendTimeoutInMilliseconds);
    }

    @Test
    public void testHealthCheck() {
        Map<TaskDefinition, Long> map = new LinkedHashMap<>();
        map.put(firstTaskDefinition, firstTaskDefinitionOffloadTime);
        map.put(secondTaskDefinition, secondTaskDefinitionOffloadTime);

        when(workerBuffer.getTaskDefinitionOffloadTimes()).thenReturn(new HashMap<>()).thenReturn(map);
        when(workerBuffer.getLastResponseTime()).thenReturn(System.currentTimeMillis() + 1000, System.currentTimeMillis() - 1001);

        healthChecker.checkWorkBuffer(workerBuffer);
        verify(workerBuffer).getTaskDefinitionOffloadTimes();

        healthChecker.checkWorkBuffer(workerBuffer);
        verify(workerBuffer, times(2)).getTaskDefinitionOffloadTimes();
        verify(workerBuffer).getLastResponseTime();
        verify(workerBuffer).clearOffloadTime(secondTaskDefinition);
        verify(workerBuffer).unsuspend();

        healthChecker.checkWorkBuffer(workerBuffer);
        verify(workerBuffer, times(2)).getLastResponseTime();
        verify(workerBuffer).suspend();
        verify(workerBuffer, times(3)).getTaskDefinitionOffloadTimes();
        verify(workerBuffer, times(2)).clearOffloadTime(secondTaskDefinition);

        healthChecker.checkWorkBuffer(workerBuffer);
        verify(workerBuffer, times(3)).getLastResponseTime();
        verify(workerBuffer, times(2)).suspend();
        verify(workerBuffer, times(4)).getTaskDefinitionOffloadTimes();
        verify(workerBuffer, times(3)).clearOffloadTime(secondTaskDefinition);

        verifyNoMoreInteractions(firstTaskDefinition, secondTaskDefinition, workerBuffer);
    }
}