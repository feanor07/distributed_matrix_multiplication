package com.dilek.alptug.worker;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Test class for {@link DefaultWorkerBuffersManager}
 *
 * @author alptugd
 */
public class DefaultWorkerBuffersManagerTest {

    private static final int MANAGEMENT_CYCLE = 200;

    @Mock
    private WorkerBufferProducer producer;

    @Mock
    private WorkerBufferProcessor consumer;

    @Mock
    private WorkerBuffer firstWorkerBuffer;

    @Mock
    private WorkerBuffer secondWorkerBuffer;

    private DefaultWorkerBuffersManager workerBuffersManager;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        workerBuffersManager = new DefaultWorkerBuffersManager(producer, consumer);
    }

    @Test
    public void testManagement() throws Exception {
       doNothing().doThrow(new WorkerBufferProducer.CannotProduceFurtherException()).when(producer).produce(firstWorkerBuffer);

       assertFalse(workerBuffersManager.isDone());
       workerBuffersManager.startManagement(Arrays.asList(firstWorkerBuffer, secondWorkerBuffer), MANAGEMENT_CYCLE);

       Thread.sleep(3 * MANAGEMENT_CYCLE);
       assertTrue(workerBuffersManager.isDone());

       verify(producer, times(2)).produce(firstWorkerBuffer);
       verify(producer, times(1)).produce(secondWorkerBuffer);
       verify(consumer, times(1)).process(firstWorkerBuffer);
       verify(consumer, times(1)).process(secondWorkerBuffer);
    }
}