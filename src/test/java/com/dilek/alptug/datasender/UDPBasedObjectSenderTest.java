package com.dilek.alptug.datasender;

import com.dilek.alptug.entity.Task;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

import static org.junit.Assert.*;

/**
 * Test class for {@link UDPBasedObjectSender}
 *
 * @author alptugd
 */
public class UDPBasedObjectSenderTest {
    private static final int[] ROW_VECTORS = new int[]{1,2,3,4};
    private static final int[] COLUMN_VECTORS = new int[]{5,6,7,8};
    private static final int ID = 1;
    private static final int VECTOR_LENGTH = 2;
    private static final int PORT = 9876;

    private UDPBasedObjectSender<Task> udpBasedTaskSender = new UDPBasedObjectSender<>();

    @Test
    public void testInvalidAddressSendingTrial() throws Exception {
        Task task = new Task();
        task.setVectorLength(VECTOR_LENGTH);
        task.setTaskId(ID);
        task.setRowVectors(ROW_VECTORS);
        task.setColumnVectors(COLUMN_VECTORS);

        assertFalse(udpBasedTaskSender.sendObject(task, new InetSocketAddress("alptug", PORT)));
    }

    @Test
    public void testSuccessfullUdpBasedTaskSending() throws Exception {
        Task task = new Task();
        task.setVectorLength(VECTOR_LENGTH);
        task.setTaskId(ID);
        task.setRowVectors(ROW_VECTORS);
        task.setColumnVectors(COLUMN_VECTORS);

        TestUdpServer server = new TestUdpServer();
        createBasicUdpServerInASeperateThread(server);
        Thread.sleep(1000);

        assertTrue(udpBasedTaskSender.sendObject(task, new InetSocketAddress("localhost", PORT)));

        while(server.isFinished) {
            Thread.sleep(200);
        }
    }

    private void createBasicUdpServerInASeperateThread(TestUdpServer server) {
        new Thread(server).start();
    }

    class TestUdpServer implements Runnable {

        private boolean isFinished = false;

        @Override
        public void run() {
            try {
                DatagramSocket serverSocket = new DatagramSocket(PORT);
                byte[] receiveData = new byte[65507];

                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);

                ByteArrayInputStream baos = new ByteArrayInputStream(receivePacket.getData());
                ObjectInputStream oos = new ObjectInputStream(baos);
                Task task = (Task) oos.readObject();

                assertArrayEquals(COLUMN_VECTORS, task.getColumnVectors());
                assertArrayEquals(ROW_VECTORS, task.getRowVectors());
                assertEquals(VECTOR_LENGTH, task.getVectorLength());
                assertEquals(ID, task.getTaskId());

                isFinished = true;
            } catch (Exception e) {
                throw new RuntimeException("Unexpected exception");
            }
        }
    }
}