package util;

/**
 * Contains static utility methods to be used across test classes.
 *
 * @author alptugd
 */
public class TestUtil {
    /**
     * Creates a double array of size row count and column count; where a value at row index 'r' and column index 'c' is
     * equal to (r*rowCount) + (c*columnCount).
     *
     * @param rowCount, number of rows
     * @param columnCount, number of columns
     * @return double array of specified size
     */
    public static int[][] createDoubleArray(int rowCount, int columnCount) {
        int[][] matrix = new int[rowCount][columnCount];

        for (int i = 0; i< rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                matrix[i][j] = i * rowCount + j * columnCount;
            }
        }
        return matrix;
    }

}
