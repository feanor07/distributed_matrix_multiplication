package com.dilek.alptug.entity;

/**
 * An in-memory representation of a matrix. This class assumes the matrix fits in the memory. Alternative implementations
 * might consider representing the matrix from a database, file, cache, etc.
 *
 * @author alptugd
 */
public class InMemoryMatrix implements Matrix {
    private static final String PARTITIONING_PARAMETERS_INVALID = "Row and column indeces must be greater than or equal to 0, " +
            "requested column and row counts must be greater than 0";
    private static final String ROW_PARTITION_PARAMETERS_EXCEED_SIZE = "Row partitoning parameters exceed matrix size";
    private static final String COLUMN_PARTITION_PARAMETERS_EXCEED_SIZE = "Column partitoning parameters exceed matrix size";
    private static final String INVALID_MATRIX = "A valid matrix has at least one row and one column and each row has equal number of elements";

    // Matrix being represented
    private final int[][] matrix;

    /**
     * Constructs an in memory matrix representation for integer double array argument.
     *
     * @param doubleArray, double array to be represented as matrix
     * @throws InvalidMatrixException when passed in array does not have rows or columns and varying size of columns
     */
    public InMemoryMatrix(int[][] doubleArray) throws InvalidMatrixException {
        checkMatrix(doubleArray);
        this.matrix = doubleArray;
    }

    /**
     * Checks the partitioning parameters whether requested sub-matrix is eligible to be created from this representation
     * and returns the sub-matrix if possible.
     *
     * @param rowIndex, the index of the row of the matrix that will be the first row of sub-matrix
     * @param columnIndex, the index of the column of the matrix that will be the first column of sub-matrix
     * @param rowCount, the number of rows of the sub-matrix
     * @param columnCount, the number of columns of the sub-matrix
     *
     * @return sub-matrix
     */
    public int[][] partition(int rowIndex, int columnIndex, int rowCount, int columnCount) {
        checkParameters(rowIndex, columnIndex, rowCount, columnCount, PARTITIONING_PARAMETERS_INVALID,
                ROW_PARTITION_PARAMETERS_EXCEED_SIZE, COLUMN_PARTITION_PARAMETERS_EXCEED_SIZE);

        int[][] partition = new int[rowCount][columnCount];

        for (int i=0; i < rowCount; i++) {
            for (int j=0; j < columnCount; j++) {
                partition[i][j] = this.matrix[i+rowIndex][j+columnIndex];
            }
        }

        return partition;
    }

    /**
     * Adds the argument submatrix to the one represented in this instance. It uses the portion of the submatrix that
     * stays within limits. Does not check the argument submatrix to be a valid matrix for performance considerations.
     *
     * @param rowIndex, row index to which addition will be started
     * @param columnIndex, column index to which addition will be started
     * @param submatrix, double array repreenting a submatrix
     */
    public void add(int rowIndex, int columnIndex, int[][] submatrix) {
        if (submatrix.length == 0 || submatrix[0].length == 0) {
            return;
        }

        int rowUpperLimit = Math.min(matrix.length, rowIndex + submatrix.length);
        int columnUpperLimit = Math.min(matrix[0].length, columnIndex + submatrix[0].length);

        for (int i=rowIndex; i < rowUpperLimit; i++) {
            for (int j=columnIndex; j < columnUpperLimit; j++) {
                matrix[i][j] += submatrix[i-rowIndex][j-columnIndex];
            }
        }
    }

    public int getValue(int rowIndex, int columnIndex) {
        return this.matrix[rowIndex][columnIndex];
    }

    @Override
    public int getRowCount() {
        return matrix.length;
    }

    @Override
    public int getColumnCount() {
        return matrix[0].length;
    }

    private void checkMatrix(int[][] matrix) throws InvalidMatrixException{
        if (matrix.length == 0 || matrix[0].length == 0) {
            throw new InvalidMatrixException();
        }

        int expectedColumnCount = matrix[0].length;

        for (int i = 1; i < matrix.length; i++) {
            if (matrix[i].length != expectedColumnCount) {
                throw new InvalidMatrixException();
            }
        }
    }

    private void checkParameters(int rowIndex, int columnIndex, int rowCount, int columnCount, String indicesAndCountMessage,
                                 String rowLimitMessage, String columnLimitMessage) {
        if (rowIndex < 0 || columnIndex < 0 || rowCount < 1 || columnCount < 1) {
            throw new IllegalArgumentException(indicesAndCountMessage);
        }

        if (rowIndex + rowCount > matrix.length) {
            throw new IllegalArgumentException(rowLimitMessage);
        }

        if (columnIndex + columnCount > matrix[0].length) {
            throw new IllegalArgumentException(columnLimitMessage);
        }
    }

    /**
     * Checked exception class used when the construct to be represented is not a matrix indeed.
     */
     static class InvalidMatrixException extends Exception {
        InvalidMatrixException() {
            super(INVALID_MATRIX);
        }
    }
}
