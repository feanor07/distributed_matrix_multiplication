package com.dilek.alptug.entity;

/**
 * This class represents a task definition through which actual tasks to be offloaded to a worker can be constructed
 * in order to perform a sub-matrix multiplication.
 *
 * @author alptugd
 */
public class TaskDefinition {
    private int resultRowIndex;
    private int rowCount;
    private int resultColumnIndex;
    private int columnCount;
    private int commonIndex;
    private int vectorLength;
    private boolean isFinished = false;
    private int id;

    /**
     * Initializes a new task definition through the builder argument.
     *
     * @param taskDefinitionBuilder task definition builder to use during initialization
     */
    TaskDefinition(TaskDefinitionBuilder taskDefinitionBuilder) {
        this.resultRowIndex = taskDefinitionBuilder.getResultRowIndex();
        this.rowCount = taskDefinitionBuilder.getRowCount();
        this.resultColumnIndex = taskDefinitionBuilder.getResultColumnIndex();
        this.columnCount = taskDefinitionBuilder.getColumnCount();
        this.commonIndex = taskDefinitionBuilder.getCommonIndex();
        this.vectorLength = taskDefinitionBuilder.getVectorLength();
        this.id = taskDefinitionBuilder.getId();
    }

    public synchronized boolean markFinished() {
        boolean resultToReturn = !this.isFinished;

        this.isFinished = true;

        return resultToReturn;
    }

    public synchronized boolean isFinished() {
        return isFinished;
    }

    public int getResultRowIndex() {
        return resultRowIndex;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getResultColumnIndex() {
        return resultColumnIndex;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public int getCommonIndex() {
        return commonIndex;
    }

    public int getVectorLength() {
        return vectorLength;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof TaskDefinition && this.id == ((TaskDefinition) obj).id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "" + getId();
    }
}
