package com.dilek.alptug.entity;

/**
 * Default builder implementation to create task definitions via successive method calls.
 *
 * @author alptugd
 */
public class DefaultTaskDefinitionBuilder implements TaskDefinitionBuilder {
    private int resultRowIndex;
    private int rowCount;
    private int resultColumnIndex;
    private int columnCount;
    private int commonIndex;
    private int vectorLength;
    private int id;

    public TaskDefinition build() {
        return new TaskDefinition(this);
    }

    public int getResultRowIndex() {
        return resultRowIndex;
    }

    public TaskDefinitionBuilder setResultRowIndex(int resultRowIndex) {
        this.resultRowIndex = resultRowIndex;

        return this;
    }

    public int getRowCount() {
        return rowCount;
    }

    public TaskDefinitionBuilder setRowCount(int rowCount) {
        this.rowCount = rowCount;

        return this;
    }

    public int getResultColumnIndex() {
        return resultColumnIndex;
    }

    public TaskDefinitionBuilder setResultColumnIndex(int resultColumnIndex) {
        this.resultColumnIndex = resultColumnIndex;

        return this;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public TaskDefinitionBuilder setColumnCount(int columnCount) {
        this.columnCount = columnCount;

        return this;
    }

    public int getCommonIndex() {
        return commonIndex;
    }

    public TaskDefinitionBuilder setCommonIndex(int commonIndex) {
        this.commonIndex = commonIndex;

        return this;
    }

    public int getVectorLength() {
        return vectorLength;
    }

    public TaskDefinitionBuilder setVectorLength(int vectorLength) {
        this.vectorLength = vectorLength;

        return  this;
    }

    public int getId() {
        return id;
    }

    public TaskDefinitionBuilder setId(int id) {
        this.id = id;

        return this;
    }
}
