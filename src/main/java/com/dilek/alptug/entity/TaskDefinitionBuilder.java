package com.dilek.alptug.entity;

/**
 * Builder interface for {@link TaskDefinition}
 *
 * @author alptugd
 */
public interface TaskDefinitionBuilder {
    TaskDefinition build();

    int getResultRowIndex();

    TaskDefinitionBuilder setResultRowIndex(int resultRowIndex);

    int getRowCount();

    TaskDefinitionBuilder setRowCount(int rowCount);

    int getResultColumnIndex();

    TaskDefinitionBuilder setResultColumnIndex(int resultColumnIndex);

    int getColumnCount();

    TaskDefinitionBuilder setColumnCount(int columnCount);

    int getCommonIndex();

    TaskDefinitionBuilder setCommonIndex(int commonIndex);

    int getVectorLength();

    TaskDefinitionBuilder setVectorLength(int vectorLength);

    int getId();

    TaskDefinitionBuilder setId(int id);
}
