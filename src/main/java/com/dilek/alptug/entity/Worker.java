package com.dilek.alptug.entity;

/**
 * This class represents a worker to offload multiplication tasks.
 *
 * @author alptugd
 */
public class Worker {
    private String ip;
    private int port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Worker) {
            Worker other = (Worker) obj;

            return other.getIp().equals(this.getIp()) && other.getPort() == this.getPort();
        }

        return false;
    }

    @Override
    public int hashCode() {
        int result = 17;

        result = 31 * result + this.getPort();
        result = 31 * result + this.getIp().hashCode();

        return  result;
    }

    @Override
    public String toString() {
        return ip + ":" + port;
    }
}
