package com.dilek.alptug.entity;

/**
 * Interface that represents an integer matrix
 *
 * @author alptugd
 */
public interface Matrix {
    /**
     * Returns a sub-matrix from the matrix being represented.
     *
     * @param rowIndex, the index of the row of the matrix that will be the first row of sub-matrix
     * @param columnIndex, the index of the column of the matrix that will be the first column of sub-matrix
     * @param rowCount, the number of rows of the sub-matrix
     * @param columnCount, the number of columns of the sub-matrix
     *
     * @return sub-matrix
     */
    int[][] partition(int rowIndex, int columnIndex, int rowCount, int columnCount);

    /**
     * Adds the double array in the argument to the specified rowIndex and columnIndex of this matrix.
     *
     * @param rowIndex, row index to which addition will be started
     * @param columnIndex, column index to which addition will be started
     * @param submatrix, double array repreenting a submatrix
     */
    void add(int rowIndex, int columnIndex, int[][] submatrix);

    /**
     * Returns the value at specified row and column indices.
     *
     * @param rowIndex, row index of desired value
     * @param columnIndex, column index of desired value
     * @return value at specified location
     */
    int getValue(int rowIndex, int columnIndex);

    /**
     * Retruns row count
     * @return row count
     */
    int getRowCount();

    /**
     * Retruns column count
     * @return column count
     */
    int getColumnCount();
}
