package com.dilek.alptug.worker;

import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.Worker;
import com.dilek.alptug.task.TaskCreator;
import com.dilek.alptug.task.TaskSender;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default {@link WorkerBufferProcessor} implementation.
 *
 * @author alptugd
 */
public class DefaultWorkerBufferProcessor implements WorkerBufferProcessor {
    private static final Logger LOGGER = Logger.getLogger(DefaultWorkerBufferProcessor.class.getName());

    private final TaskCreator taskCreator;

    private final TaskSender taskSender;

    private WorkerBufferHealthChecker healthChecker;

    private final Matrix first;

    private final Matrix second;

    public DefaultWorkerBufferProcessor(TaskCreator taskCreator, TaskSender taskSender, WorkerBufferHealthChecker healthChecker,
                                        Matrix first, Matrix second) {
        this.taskCreator = taskCreator;
        this.taskSender = taskSender;
        this.first = first;
        this.second = second;
        this.healthChecker = healthChecker;
    }

    @Override
    public void process(WorkerBuffer workerBuffer) {
        healthChecker.checkWorkBuffer(workerBuffer);

        LOGGER.log(Level.INFO, "Removing finished task definitions for " + workerBuffer);
        workerBuffer.removeFinishedTaskDefinitions();

        Optional<TaskDefinition> optionalTaskDefinition = workerBuffer.requestTaskDefinition();
        Worker worker = workerBuffer.getWorker();

        while (optionalTaskDefinition.isPresent()) {
            TaskDefinition taskDefinition = optionalTaskDefinition.get();
            createAndSendTaskInASeperateThread(taskDefinition, worker);
            optionalTaskDefinition = workerBuffer.requestTaskDefinition();
        }
    }

    private void createAndSendTaskInASeperateThread(TaskDefinition taskDefinition, Worker worker) {
        new Thread(()->{
            LOGGER.log(Level.INFO, "Task definition " + taskDefinition + " is about to be converted to task and offloaded to worker with properties " + worker);

            Task task = taskCreator.createTask(taskDefinition, first, second);
            taskSender.sendTask(task, worker);
        }).start();
    }
}
