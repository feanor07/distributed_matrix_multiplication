package com.dilek.alptug.worker;

import java.util.Collection;
import java.util.Iterator;

/**
 * Default {@link WorkerBuffersManager} implementation.
 *
 * @author alptugd
 */
public class DefaultWorkerBuffersManager implements WorkerBuffersManager {

    private final WorkerBufferProducer workerBufferProducer;
    private final WorkerBufferProcessor workerBufferProcessor;
    private int managementCycleInMilliseconds;
    private Collection<WorkerBuffer> workerBuffers;
    private boolean isDone;

    public DefaultWorkerBuffersManager(WorkerBufferProducer workerBufferProducer, WorkerBufferProcessor workerBufferProcessor) {
        this.workerBufferProducer = workerBufferProducer;
        this.workerBufferProcessor = workerBufferProcessor;
        this.isDone = false;
    }

    @Override
    public void startManagement(Collection<WorkerBuffer> workerBuffers, int managementCycleInMilliseconds) {
        this.workerBuffers = workerBuffers;
        this.managementCycleInMilliseconds = managementCycleInMilliseconds;
        startManagementThread();
    }

    /**
     * Management thread is iterated until worker buffer producer ends up with nothing to produce anymore.
     */
    private void startManagementThread() {
        new Thread(()->{
            while (!isDone) {
                try {
                    Iterator<WorkerBuffer> iterator = workerBuffers.iterator();

                    while (iterator.hasNext()) {
                        WorkerBuffer workerBuffer = iterator.next();

                        try {
                            workerBufferProducer.produce(workerBuffer);
                        } catch (WorkerBufferProducer.CannotProduceFurtherException e) {
                            isDone = true;
                            break;
                        }

                        workerBufferProcessor.process(workerBuffer);
                    }

                    Thread.sleep(this.managementCycleInMilliseconds);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public boolean isDone() {
        return isDone;
    }
}
