package com.dilek.alptug.worker;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.Worker;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Interface representing workload buffer to be offloaded to a worker.
 *
 * @author alptugd
 */
public interface WorkerBuffer {

    /**
     * Retruns number of available slots within the buffer.
     *
     * @return available space in the buffer
     */
    int getFreeSpace();

    /**
     * Puts task definitions as the new workload to the buffer.
     *
     * @param taskDefinitions, task definitions to be buffered as new workload
     * @return number of task definitions buffered
     */
    int putTaskDefinitions(Collection<TaskDefinition> taskDefinitions);

    /**
     * Requests a task definition from the buffer if an eligeble one exits.
     *
     * @return an appropriate task definition if exists
     */
    Optional<TaskDefinition> requestTaskDefinition();

    /**
     * Suspends the buffer from accepting new task definitions.
     */
    void suspend();

    /**
     * Unsuspends the buffer so that new task definitions are welcome.
     */
    void unsuspend();

    /**
     * Returns whether or not the buffer is suspended.
     *
     * @return true if buffer is currently suspended
     */
    boolean isSuspended();

    /**
     * Returns the worker this buffer is dedicated to.
     *
     * @return associated worker
     */
    Worker getWorker();

    /**
     * Returns offloading times of task definitions currently existing in this buffer
     * @return a map containing task definitions along with offload time in milliseconds.
     */
    Map<TaskDefinition, Long> getTaskDefinitionOffloadTimes();

    /**
     * Removes the finished task definitions from the buffer so as to open space for new task definitions to come
     */
    void removeFinishedTaskDefinitions();

    /**
     * Sets the last response time for this buffer
     * @param lastResponseTime for this buffer
     */
    void setLastResponseTime(long lastResponseTime);

    /**
     * Returns the last response time for this buffer, implementations are expected to return creation time if no response is yet received.
     *
     * @return last task put time
     */
    long getLastResponseTime();

    /**
     * Return all available task definitions
     *
     * @return all task definitions within this buffer
     */
    Collection<TaskDefinition> getAllTaskDefinitions();

    /**
     * This method clears the offload time for this task definition
     * @param taskDefinition for which offload time will be cleared
     */
    void clearOffloadTime(TaskDefinition taskDefinition);
}
