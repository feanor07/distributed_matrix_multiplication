package com.dilek.alptug.worker;

import com.dilek.alptug.entity.Worker;

import java.util.Collection;

/**
 * Functional interface to create collection of {@link Worker}s from the given source
 *
 * @author alptugd
 */
public interface WorkerCreator<T> {
    Collection<Worker> createWorkers(T source);
}
