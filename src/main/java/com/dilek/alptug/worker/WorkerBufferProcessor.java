package com.dilek.alptug.worker;

/**
 * Functional interface responsible for processing from a {@link WorkerBuffer}.
 *
 * @author alptugd
 */
public interface WorkerBufferProcessor {
    void process(WorkerBuffer workerBuffer);
}

