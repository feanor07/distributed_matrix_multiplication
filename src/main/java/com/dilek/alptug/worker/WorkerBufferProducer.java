package com.dilek.alptug.worker;

/**
 * Functional interface responsible for producing to a {@link WorkerBuffer}.
 *
 * @author alptugd
 */
public interface WorkerBufferProducer {
    /**
     * Implementers are expected to throw CannotProduceFurtherException when they are done producing.
     *
     * @param workerBuffer, worker buffer to produce to.
     * @throws CannotProduceFurtherException when there is nothing to produce any more
     */
    void produce(WorkerBuffer workerBuffer) throws CannotProduceFurtherException;

    class CannotProduceFurtherException extends Exception {
    }
}
