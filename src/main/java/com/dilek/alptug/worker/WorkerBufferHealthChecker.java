package com.dilek.alptug.worker;

/**
 * Interface responsible to check the health status of the given work buffer. Implementors are typically expected to iterate
 * over existing task definitions and update the status of the task definitions and/or the buffer.
 *
 * @author alptugd
 */
public interface WorkerBufferHealthChecker {
    void checkWorkBuffer(WorkerBuffer buffer);
}
