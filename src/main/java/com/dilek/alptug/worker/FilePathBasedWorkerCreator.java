package com.dilek.alptug.worker;

import com.dilek.alptug.entity.Worker;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

/**
 * A file based {@link WorkerCreator} implementation where source is the path of the file
 * containing the information for workers.
 *
 * @author alptugd
 */
public class FilePathBasedWorkerCreator implements WorkerCreator<String> {
    private static final String SEPARATOR = ":";
    private static final String IP_SEPARATOR = "\\.";

    @Override
    public Collection<Worker> createWorkers(String source) {
        Path path = FileSystems.getDefault().getPath(source);

        try {
            Stream<String> lines = Files.lines(path);
            List<Worker> workers = new ArrayList<>();

            lines.forEach((line)-> {
                Worker worker = new Worker();
                String[] tokens = line.split(SEPARATOR);
                worker.setIp(tokens[0]);
                checkIp(worker.getIp());
                worker.setPort(Integer.parseInt(tokens[1]));
                workers.add(worker);
            });

            return  workers;

        } catch (Exception e) {
            throw new RuntimeException("Invalid worker file", e);
        }
    }

    private void checkIp(String ip) {
        String tokens[] = ip.split(IP_SEPARATOR);

        if (tokens.length != 4) {
            throw new IllegalArgumentException("Invalid ip provided " + ip);
        }

        Integer.parseInt(tokens[0]);
        Integer.parseInt(tokens[1]);
        Integer.parseInt(tokens[2]);
        Integer.parseInt(tokens[3]);

    }
}
