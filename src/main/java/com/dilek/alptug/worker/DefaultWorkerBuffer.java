package com.dilek.alptug.worker;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.Worker;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Default {@link WorkerBuffer} implementation.
 *
 * @author alptugd
 */
public class DefaultWorkerBuffer implements WorkerBuffer {

    private final int capacity;
    private boolean active;
    private final Worker worker;
    private final Map<Integer, TaskDefinition> taskDefinitionMap;
    private final Map<TaskDefinition, Long> taskDefinitionOffloadTimes;
    private long lastResponseTime = 0;

    public DefaultWorkerBuffer(int capacity, Worker worker) {
        this.active = true;
        this.worker = worker;
        this.capacity = capacity;
        this.taskDefinitionMap = new ConcurrentHashMap<>(capacity);
        this.taskDefinitionOffloadTimes = new ConcurrentHashMap<>(capacity);
        this.lastResponseTime = System.currentTimeMillis();
    }

    @Override
    public Worker getWorker() {
        return worker;
    }

    @Override
    public Map<TaskDefinition, Long> getTaskDefinitionOffloadTimes() {
        return taskDefinitionOffloadTimes;
    }

    @Override
    public void removeFinishedTaskDefinitions() {

        Iterator<Map.Entry<Integer, TaskDefinition>> iterator = taskDefinitionMap.entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry<Integer, TaskDefinition> entry = iterator.next();

            if (entry.getValue().isFinished()) {
                iterator.remove();
                taskDefinitionOffloadTimes.remove(entry.getValue());
            }
        }
    }

    @Override
    public void setLastResponseTime(long lastResponseTime) {
        this.lastResponseTime = lastResponseTime;
    }

    @Override
    public long getLastResponseTime() {
        return lastResponseTime;
    }

    @Override
    public Collection<TaskDefinition> getAllTaskDefinitions() {
        return taskDefinitionMap.values();
    }

    @Override
    public void clearOffloadTime(TaskDefinition taskDefinition) {
        taskDefinitionOffloadTimes.remove(taskDefinition);
    }

    public int getFreeSpace() {
        return this.active ? this.capacity - this.taskDefinitionMap.size() : 0;
    }

    public boolean isSuspended() {
        return !this.active;
    }

    public int putTaskDefinitions(Collection<TaskDefinition> taskDefinitions) {
        if (!this.active) {
            return 0;
        }

        int freeSpace = getFreeSpace();
        taskDefinitions.stream().limit(freeSpace).forEach(item->this.taskDefinitionMap.put(item.getId(), item));

        return Math.min(freeSpace, taskDefinitions.size());
    }

    public Optional<TaskDefinition> requestTaskDefinition() {
        Optional<TaskDefinition> result = this.taskDefinitionMap.values().stream().filter(taskDefinition ->
            taskDefinitionOffloadTimes.get(taskDefinition)==null && !taskDefinition.isFinished()
        ).findFirst();
        result.ifPresent((taskDefinition -> taskDefinitionOffloadTimes.put(taskDefinition, System.currentTimeMillis())));

        return  result;
    }

    public void suspend() {
        this.active = false;
    }

    public void unsuspend() {
        this.active = true;
    }

    @Override
    public String toString() {
        return "Buffer for worker with properties; " + worker.toString();
    }
}
