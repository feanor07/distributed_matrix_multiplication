package com.dilek.alptug.worker;

import java.util.Collection;

/**
 * Interface responsible for management of producing to and consuming from collection of {@link WorkerBuffer}s
 *
 * @author alptugd
 */
public interface WorkerBuffersManager {
    /**
     * This method initiates management of worker buffers in a cycle of specified milliseconds until finished
     *
     * @param workerBuffers, worker buffers to manage
     * @param managementCycleInMilliseconds, periodic management time in milliseconds
     */
    void startManagement(Collection<WorkerBuffer> workerBuffers, int managementCycleInMilliseconds);

    /**
     * This method returns a flag to indicate whether the management of the worker buffers is finalized.
     *
     * @return whether management task is finished
     */
    boolean isDone();
}
