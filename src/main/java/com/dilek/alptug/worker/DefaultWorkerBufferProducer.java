package com.dilek.alptug.worker;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.taskdefinition.TaskDefinitionProvider;
import com.dilek.alptug.taskdefinition.TaskDefinitionRepository;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default {@link WorkerBufferProducer} implementation.
 *
 * @author alptugd
 */
public class DefaultWorkerBufferProducer implements WorkerBufferProducer {

    private static final Logger LOGGER = Logger.getLogger(DefaultWorkerBufferProducer.class.getName());

    private final TaskDefinitionProvider taskDefinitionProvider;
    private final TaskDefinitionRepository repository;

    public DefaultWorkerBufferProducer(TaskDefinitionProvider taskDefinitionProvider, TaskDefinitionRepository repository) {
        this.taskDefinitionProvider = taskDefinitionProvider;
        this.repository = repository;
    }

    @Override
    public void produce(WorkerBuffer workerBuffer) throws CannotProduceFurtherException {
        if (taskDefinitionProvider.isDonePartitioning()) {
            throw new CannotProduceFurtherException();
        }

        int freeSpace = workerBuffer.getFreeSpace();

        if (freeSpace > 0) {
            Collection<TaskDefinition> taskDefinitions = taskDefinitionProvider.requestTaskDefinitions(freeSpace);

            taskDefinitions.stream().forEach((taskDefinition -> LOGGER.log(Level.INFO, "Produced task definition with id " + taskDefinition.getId())));

            repository.persist(taskDefinitions);

            workerBuffer.putTaskDefinitions(taskDefinitions);
        }
    }
}
