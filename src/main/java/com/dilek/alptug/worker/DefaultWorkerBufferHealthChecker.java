package com.dilek.alptug.worker;

import com.dilek.alptug.entity.TaskDefinition;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default {@link WorkerBufferHealthChecker} implementation.
 *
 * @author alptugd
 */
public class DefaultWorkerBufferHealthChecker implements WorkerBufferHealthChecker {
    private static final Logger LOGGER = Logger.getLogger(DefaultWorkerBufferHealthChecker.class.getName());

    private final int taskDefinitionSendTimeoutInMilliseconds;
    private final int bufferSuspendTimeout;

    public DefaultWorkerBufferHealthChecker(int taskDefinitionSendTimeoutInMilliseconds, int bufferSuspendTimeout) {
        this.taskDefinitionSendTimeoutInMilliseconds = taskDefinitionSendTimeoutInMilliseconds;
        this.bufferSuspendTimeout = bufferSuspendTimeout;
    }

    @Override
    public void checkWorkBuffer(WorkerBuffer buffer) {
        Map<TaskDefinition, Long> taskDefinitionOffloadTimes = buffer.getTaskDefinitionOffloadTimes();

        if (!taskDefinitionOffloadTimes.isEmpty()) {
            changeSuspendStatusOfTheBuffer(buffer);
            clearOffloadTimeForTimedoutTaskDefinitions(buffer, taskDefinitionOffloadTimes);
        }
    }

    /**
     * Iterates over t ask definitions and unallocates the ones that has timed out (more than taskDefinitionSendTimeoutInMilliseconds had passed since offloading)
     * @param taskDefinitionOffloadTimes, map containing latest offload times for task definitions
     */
    private void clearOffloadTimeForTimedoutTaskDefinitions(WorkerBuffer buffer, Map<TaskDefinition, Long> taskDefinitionOffloadTimes) {
        taskDefinitionOffloadTimes.entrySet().stream().forEach((entry)-> {
            if (entry.getValue() + taskDefinitionSendTimeoutInMilliseconds < System.currentTimeMillis()) {
                LOGGER.log(Level.INFO, "Clearing offload time for task definition with id " + entry.getKey());

                buffer.clearOffloadTime(entry.getKey());
            }
        });
    }

    /**
     * Suspends the buffer if no response for this buffer has been received for bufferSuspendTimeout milliseconds
     * @param buffer, buffer to suspend/unsuspend
     */
    private void changeSuspendStatusOfTheBuffer(WorkerBuffer buffer) {
        long lastResponseTime = buffer.getLastResponseTime();

        if (lastResponseTime + bufferSuspendTimeout < System.currentTimeMillis()) {
            LOGGER.log(Level.INFO, "Suspending " + buffer);
            buffer.suspend();
        } else {
            LOGGER.log(Level.INFO, "Unsuspending " + buffer);
            buffer.unsuspend();
        }
    }
}

