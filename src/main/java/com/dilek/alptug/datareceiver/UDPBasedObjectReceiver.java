package com.dilek.alptug.datareceiver;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.function.Consumer;

/**
 * UDP protocol based {@link ObjectReceiver} implementation.
 *
 * @author alptugd
 */
public class UDPBasedObjectReceiver<T extends Serializable> implements ObjectReceiver<T> {
    private static final int MAX_UDP_PACKAGE_SIZE = 65507;
    private int socketTimeOutExceptionInMillis;

    public UDPBasedObjectReceiver() {
        this(0);
    }

    public UDPBasedObjectReceiver(int socketTimeOutExceptionInMillis) {
        this.socketTimeOutExceptionInMillis = socketTimeOutExceptionInMillis;
    }

    /**
     * Infinitely loops for receiving from the retrieved port and blocks for receiving objects.
     */
    @Override
    public void startReceiving(int port, Consumer<T> consumer) {
        DatagramSocket serverSocket = null;

        try {
            serverSocket = new DatagramSocket(port);
            serverSocket.setSoTimeout(socketTimeOutExceptionInMillis);
            byte[] receiveData = new byte[MAX_UDP_PACKAGE_SIZE];

            while(true) {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);

                ByteArrayInputStream baos = new ByteArrayInputStream(receivePacket.getData());
                ObjectInputStream oos = new ObjectInputStream(baos);
                T data = (T) oos.readObject();

                consumer.accept(data);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (serverSocket != null) {
                serverSocket.close();
            }
        }

    }
}
