package com.dilek.alptug.datareceiver;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 * Functional interface for receiving objects from a specified port and consuming the retrieved object
 */
public interface ObjectReceiver<T extends Serializable> {
    /**
     * This method is expected to start the object receival process from the given port and delegate
     * the consuming of the retrieved object to the consumer.
     *
     * @param port, port to receive object(s) from
     * @param consumer, consumer to process retrieved object(s)
     */
    void startReceiving(int port, Consumer<T> consumer);
}
