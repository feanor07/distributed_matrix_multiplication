package com.dilek.alptug.datasender;

import java.io.Serializable;
import java.net.InetSocketAddress;

/**
 * Functional interface responsible for sending an object to the specified address.
 *
 * @author alptugd
 */
public interface ObjectSender<T extends Serializable> {
    boolean sendObject(T object, InetSocketAddress address);
}
