package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinitionBuilder;

/**
 * Factory interface for {@link TaskDefinitionBuilder}
 *
 * @author alptugd
 */
public interface TaskDefinitionBuilderFactory {
    TaskDefinitionBuilder newMatrixPartitionDefinitionBuilder();
}
