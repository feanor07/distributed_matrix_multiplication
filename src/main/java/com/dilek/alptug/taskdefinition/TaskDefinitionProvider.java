package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;

import java.util.Collection;

/**
 * Interface enabling provision of task definitions to be offloaded to workers.
 *
 * @author alptugd
 */
public interface TaskDefinitionProvider {

    /**
     * Creates requested number of task definitions if possible or maximum available.
     *
     * @param count, number of task definitions requested to be created
     * @return collection of task definitions created in regards to initialization parameters
     */
    Collection<TaskDefinition> requestTaskDefinitions(int count);

    /**
     * Returns whether the partioning is finished.
     *
     * @return true if partioning is already finished and there is no more task definition to create
     */
    boolean isDonePartitioning();
}
