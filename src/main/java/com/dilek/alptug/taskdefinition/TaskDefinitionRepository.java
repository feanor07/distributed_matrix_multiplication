package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;

import java.util.Collection;

/**
 * Interface for managing {@link TaskDefinition} related operations.
 *
 * @author alptugd
 */
public interface TaskDefinitionRepository {

    /**
     * Persists given task definitions to underlying data strucutre
     *
     * @param taskDefinitions, task definitions to persist
     */
    void persist(Collection<TaskDefinition> taskDefinitions);

    /**
     * Marks the task definition as finished
     * @param taskDefinitionId, id of the task definition to mark as finished
     * @return whether task definition is marked as finished with this invocation
     */
    boolean markTaskDefinitionAsFinished(int taskDefinitionId);

    /**
     * Returns the task definition with id
     * @param taskDefinitionId, id of the task definition to return
     * @return taskDefinition with given id
     */
    TaskDefinition getTaskDefinition(int taskDefinitionId);

    /**
     * Returns whether all task definitions within repository is finished
     * @return true in case all task definitions are finished, false otherwise
     */
    boolean areAllTaskDefinitionsFinished();

}
