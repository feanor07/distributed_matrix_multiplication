package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.worker.WorkerBuffer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * This class iterates over worker buffers and retrieves task definitions from suspended ones so as to offload the same tasks
 * to remaining active buffers. It returns just one task definition no matter how muck task definition is requested in order
 * to provide a best-effort even distribution to all active worker buffers.
 *
 * @author alptugd
 */
public class RemainingTaskDefinitionProvider implements TaskDefinitionProvider {

    private final TaskDefinitionRepository taskDefinitionRepository;
    private final Collection<WorkerBuffer> workerBuffers;
    private List<TaskDefinition> remainingTaskDefinitions;

    public RemainingTaskDefinitionProvider(TaskDefinitionRepository taskDefinitionRepository, Collection<WorkerBuffer> workerBuffers) {
        this.taskDefinitionRepository = taskDefinitionRepository;
        this.workerBuffers = workerBuffers;
        this.remainingTaskDefinitions = new ArrayList<>();
    }

    @Override
    public Collection<TaskDefinition> requestTaskDefinitions(int count) {
        List<TaskDefinition> result = getRemainingTaskDefinitions();

        if (result.isEmpty()) {
            return Collections.emptyList();
        }

        return  Collections.singleton(result.remove(0));
    }

    private List<TaskDefinition> getRemainingTaskDefinitions() {
        if (remainingTaskDefinitions.isEmpty()) {
            workerBuffers.stream().forEach((workerBuffer -> {
                if (workerBuffer.isSuspended()) {
                    remainingTaskDefinitions.addAll(workerBuffer.getAllTaskDefinitions());
                }
            }));
        }

        return remainingTaskDefinitions;
    }

    @Override
    public boolean isDonePartitioning() {
        return taskDefinitionRepository.areAllTaskDefinitionsFinished();
    }
}
