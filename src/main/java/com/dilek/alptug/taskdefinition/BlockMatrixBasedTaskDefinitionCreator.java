package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.TaskDefinitionBuilder;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * A block matrix multiplication based task definition creator implementation. This class tries to create equal sized
 * sub-matrices both from the first and second matrix. It partitions the first matrix's row count, second matrix's
 * column count and the common count (first one's column count and second one's row count).
 *
 * @see <a href="https://en.wikipedia.org/wiki/Block_matrix">Block Matrix</a>
 *
 * @author alptugd
 */
public class BlockMatrixBasedTaskDefinitionCreator implements TaskDefinitionProvider {

    // Starting task id to differentiate among tasks that are running separately in different application contexts
    private static int STARTING_TASK_ID = -9999;

    // Increment value of task ids for every construction
    private static int TASK_ID_INCREMENT = 10000;

    // First matrix's row size
    private int rowSize;

    // First matrix's column size and second matrix's row size
    private int commonSize;

    // Second matrix's column size
    private int columnSize;

    // Current row index of the result matrix to which sub-matrices to be created will be written
    private int resultRowIndex = 0;

    // Current column index of the result matrix to which sub-matrices to be created will be written
    private int resultColumnIndex = 0;

    // Index of first's matrix current column partition and second matrix's current row partition
    private int commonIndex = 0;

    // Number of remaining row partitions of first matrix to create task definitions
    private int remainingRowPartitionCount;

    // Number of remaining column partitions of second matrix to create task definitions
    private int remainingColumnPartitionCount;

    // Number of remaining column partitions of first matrix and row partitions of second matrix to create task definitions
    private int remainingCommonPartitionCount;

    // Total number of row partitions to create from the first matrix
    private int rowPartitionCount;

    // Total number of column partitions to create from the second matrix
    private int columnPartitionCount;

    // Total number of column partitions to create from the first matrix and row partitions to create from the second matrix
    private int commonPartitionCount;

    // Number of task definitions already created
    private int taskDefinitionAlreayCreated = 0;

    // Number of row counts from the first matrix to include to the current task definition to create
    private int currentRowCount;

    // Number of column counts from the second matrix to include to the current task definition to create
    private int currentColumnCount;

    // Length of the rows from the first matrix's rows and second matrix's columns to include to the current task definition to create
    private int currentVectorLength;

    // Factory to create the builders to construct task definitions
    private TaskDefinitionBuilderFactory taskDefinitionBuilderFactory;

    // Current task id to assign to the next task to be created
    private int currentTaskId;

    /**
     * Constructs a task definition creator
     *
     * @param taskDefinitionBuilderFactory, factory to get the task definition builder to be used during task definition
     *                                      creation
     */
    public BlockMatrixBasedTaskDefinitionCreator(TaskDefinitionBuilderFactory taskDefinitionBuilderFactory, int rowSize, int commonSize, int columnSize, int maxSquareMatrixSize) {
        incrementStartingTaskId();
        this.currentTaskId = STARTING_TASK_ID;
        this.taskDefinitionBuilderFactory = taskDefinitionBuilderFactory;
        this.rowSize = rowSize;
        this.commonSize = commonSize;
        this.columnSize = columnSize;
        this.rowPartitionCount = calculatePartitionCount(rowSize, maxSquareMatrixSize);
        this.columnPartitionCount = calculatePartitionCount(columnSize, maxSquareMatrixSize);
        this.commonPartitionCount = calculatePartitionCount(commonSize, maxSquareMatrixSize);
        this.remainingColumnPartitionCount = this.columnPartitionCount;
        this.remainingCommonPartitionCount = this.commonPartitionCount;
        this.remainingRowPartitionCount = this.rowPartitionCount;
    }

    /**
     * In order to differentiate different multiplication contexts, task definitions are tried to be given unique ids
     */
    private void incrementStartingTaskId() {
        if (Integer.MAX_VALUE - STARTING_TASK_ID < 2 * TASK_ID_INCREMENT) {
            STARTING_TASK_ID = 1;
        } else {
            STARTING_TASK_ID += TASK_ID_INCREMENT;
        }
    }

    /**
     * Calculates remaining number of task definitions to create in regards to initialization parameters and returns either
     * requested number of task definitions or remaining ones.
     *
     * @param count, number of task definitions requested to be created
     * @return Collection of task definitions
     */
    public Collection<TaskDefinition> requestTaskDefinitions(int count) {
        int remainingTasksToCreate = getRemainingTaskDefinitionsToCreate();
        int numberOfDefsToCreate = count > remainingTasksToCreate ? remainingTasksToCreate : count;

        Stream<TaskDefinition> taskDefinitions = IntStream.range(0, numberOfDefsToCreate).mapToObj(i -> {
            TaskDefinitionBuilder builder = taskDefinitionBuilderFactory.newMatrixPartitionDefinitionBuilder().setResultRowIndex(
                    resultRowIndex).setRowCount(calculateRowCount()).setResultColumnIndex(resultColumnIndex)
                    .setColumnCount(calculateColumnCount()).setCommonIndex(commonIndex)
                    .setVectorLength(calculateVectorLength()).setId(incrementTaskDefinitionAndReturnId());
            return builder.build();
        });

        return taskDefinitions.collect(Collectors.toList());
    }

    /**
     * Uses internal task definition identifier index to check whether or not there is any remaining task definition to create
     *
     * @return true if already created available number of task definitions
     */
    public boolean isDonePartitioning() {
        return getRemainingTaskDefinitionsToCreate() == 0;
    }

    /**
     * Calculates available partitions for the specified size to be partitioned
     *
     * @param sizeToBePartitioned, number to partition; first or seconds matrix's row or column length
     * @param maxPartitionLength, maximum allowed partition length
     * @return number of almost equal-length partitions
     */
    private int calculatePartitionCount(int sizeToBePartitioned, int maxPartitionLength) {
        return (int) Math.ceil((double)sizeToBePartitioned / maxPartitionLength);
    }

    /**
     * Returns remaining number of partitions to create
     *
     * @return task definitions yet to create
     */
    private int getRemainingTaskDefinitionsToCreate() {
        return rowPartitionCount * columnPartitionCount * commonPartitionCount - taskDefinitionAlreayCreated;
    }

    /**
     * Sets and returns the number of second matrix's columns to include in the next task definition to create
     *
     * @return next task definition's column count
     */
    private int calculateColumnCount() {
        currentColumnCount = (columnSize - resultColumnIndex) / remainingColumnPartitionCount;

        return currentColumnCount;
    }

    /**
     * Sets and returns the number of first matrix's rows to include in the next task definition to create
     *
     * @return next task definition's row count
     */
    private int calculateRowCount() {
        currentRowCount = (rowSize - resultRowIndex) / remainingRowPartitionCount;

        return  currentRowCount;
    }

    /**
     * Sets and returns the length of vectors (first matrix's rows' length and second matrix's columns' length) to include
     * in next task definition to create
     *
     * @return next task definition's vector length
     */
    private int calculateVectorLength() {
        currentVectorLength =  (commonSize  - commonIndex) / remainingCommonPartitionCount;

        return currentVectorLength;
    }

    /**
     * Updates partitioning indices appropriately and increments number of task definitions already created.
     *
     * @return id to assign the task definition being built
     */
    private int incrementTaskDefinitionAndReturnId() {
        incrementPartitioningIndices();
        ++taskDefinitionAlreayCreated;
        return currentTaskId++;
    }

    /**
     * Increments partitioning indices after each task definition. It first partitions the first matrix's current row and
     * second matrix's current column. It then passes over columns of the second matrix, and when all columns of second matrix
     * is partitioned with the current row of first matrix it passes to the next row of the first matrix.
     */
    private void incrementPartitioningIndices() {
        commonIndex = (currentVectorLength + commonIndex) % commonSize;

        if (--remainingCommonPartitionCount == 0) {
            remainingCommonPartitionCount = commonPartitionCount;
            resultColumnIndex = (currentColumnCount + resultColumnIndex) % columnSize;

            if (--remainingColumnPartitionCount == 0) {
                remainingColumnPartitionCount = columnPartitionCount;
                resultRowIndex = (currentRowCount + resultRowIndex) % rowSize;

                remainingRowPartitionCount--;
            }
        }
    }
}
