package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinitionBuilder;
import com.dilek.alptug.entity.DefaultTaskDefinitionBuilder;

/**
 * Default factory to create a default task definition builder. See {@link DefaultTaskDefinitionBuilder}
 *
 * @author alptugd
 */
public class DefaultTaskDefinitionBuilderFactory implements TaskDefinitionBuilderFactory {
    @Override
    public TaskDefinitionBuilder newMatrixPartitionDefinitionBuilder() {
        return new DefaultTaskDefinitionBuilder();
    }
}
