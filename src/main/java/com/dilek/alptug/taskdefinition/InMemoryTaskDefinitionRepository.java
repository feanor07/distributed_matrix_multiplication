package com.dilek.alptug.taskdefinition;

import com.dilek.alptug.entity.TaskDefinition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * An in-memory representation of {@link TaskDefinitionRepository}.
 *
 * @author alptugd
 */
public class InMemoryTaskDefinitionRepository implements TaskDefinitionRepository {

    private Map<Integer, TaskDefinition> taskDefinitionsMap = new HashMap<>();

    @Override
    public void persist(Collection<TaskDefinition> taskDefinitions) {
        taskDefinitions.stream().forEach((taskDefinition)->taskDefinitionsMap.put(taskDefinition.getId(), taskDefinition));
    }

    @Override
    public boolean markTaskDefinitionAsFinished(int taskDefinitionId) {
        TaskDefinition taskDefinition = taskDefinitionsMap.get(taskDefinitionId);
        return taskDefinition.markFinished();
    }

    @Override
    public TaskDefinition getTaskDefinition(int taskDefinitionId) {
        return this.taskDefinitionsMap.get(taskDefinitionId);
    }

    @Override
    public boolean areAllTaskDefinitionsFinished() {
        Optional<TaskDefinition> any = taskDefinitionsMap.values().stream().filter((taskDefinition -> !taskDefinition.isFinished())).findAny();
        return !any.isPresent();
    }
}
