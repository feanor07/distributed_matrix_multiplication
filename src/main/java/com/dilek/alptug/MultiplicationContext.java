package com.dilek.alptug;

import com.dilek.alptug.datareceiver.UDPBasedObjectReceiver;
import com.dilek.alptug.datasender.UDPBasedObjectSender;
import com.dilek.alptug.entity.InMemoryMatrix;
import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.Worker;
import com.dilek.alptug.matrix.FilePathBasedMatrixCreator;
import com.dilek.alptug.matrix.FilePathBasedMatrixWriter;
import com.dilek.alptug.result.*;
import com.dilek.alptug.task.DefaultTaskCreator;
import com.dilek.alptug.task.DefaultTaskSender;
import com.dilek.alptug.task.TaskCreator;
import com.dilek.alptug.task.TaskSender;
import com.dilek.alptug.taskdefinition.*;
import com.dilek.alptug.worker.*;

import java.net.InetAddress;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class is the multiplication context that is raised upon every time a matrix multiplication is required.
 *
 * @author alptugd
 */
public class MultiplicationContext implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(MultiplicationContext.class.getName());

    private final String firstMatrixPath;
    private final String secondMatrixPath;
    private String hostsFilePath;
    private final int workerBufferCapacity;
    private final int replyPort;
    private final int maxSquareMatrixSize;
    private final int workerBuffersManagementCycleInMilliseconds;
    private final int resultBufferConsumeCycleInMilliseconds;
    private final String resultMatrixPath;
    private final int taskDefinitionSendTimeoutInMilliseconds;
    private final int bufferSuspendTimeoutInMilliseconds;
    private final int objectReceiverTimeoutInMilliseconds;

    MultiplicationContext(String firstMatrixPath, String secondMatrixPath, String hostsFilePath, String resultMatrixPath, int workerBufferCapacity,
                          int replyPort, int maxSquareMatrixSize, int workerBuffersManagementCycleInMilliseconds,
                          int resultBufferConsumeCycleInMilliseconds, int taskDefinitionSendTimeoutInMilliseconds,
                          int bufferSuspendTimeoutInMilliseconds, int objectReceiverTimeoutInMilliseconds) {
        this.firstMatrixPath = firstMatrixPath;
        this.secondMatrixPath = secondMatrixPath;
        this.hostsFilePath = hostsFilePath;
        this.workerBufferCapacity = workerBufferCapacity;
        this.replyPort = replyPort;
        this.maxSquareMatrixSize = maxSquareMatrixSize;
        this.workerBuffersManagementCycleInMilliseconds = workerBuffersManagementCycleInMilliseconds;
        this.resultBufferConsumeCycleInMilliseconds = resultBufferConsumeCycleInMilliseconds;
        this.resultMatrixPath = resultMatrixPath;
        this.taskDefinitionSendTimeoutInMilliseconds = taskDefinitionSendTimeoutInMilliseconds;
        this.bufferSuspendTimeoutInMilliseconds = bufferSuspendTimeoutInMilliseconds;
        this.objectReceiverTimeoutInMilliseconds = objectReceiverTimeoutInMilliseconds;
    }

    @Override
    public void run() {
        try {
            FilePathBasedMatrixCreator matrixCreator = new FilePathBasedMatrixCreator();

            LOGGER.log(Level.INFO, "Matrices are to be loaded to memory from the provided files");
            Matrix firstMatrix = matrixCreator.createMatrixFromSource(firstMatrixPath);
            Matrix secondMatrix = matrixCreator.createMatrixFromSource(secondMatrixPath);
            Matrix resultMatrix = new InMemoryMatrix(new int[firstMatrix.getRowCount()][secondMatrix.getColumnCount()]);

            if (firstMatrix.getColumnCount() != secondMatrix.getRowCount()) {
                throw new IllegalArgumentException("Provided matrices cannot be multiplied");
            }

            LOGGER.log(Level.INFO, "Workers are to be loaded to memory from the provided file");
            Collection<Worker> workers = new FilePathBasedWorkerCreator().createWorkers(hostsFilePath);

            Collection<WorkerBuffer> workerBuffers = workers.stream().map((worker -> new DefaultWorkerBuffer(
                    workerBufferCapacity, worker))).collect(Collectors.toList());

            String myIp = InetAddress.getLocalHost().getHostAddress();

            BlockMatrixBasedTaskDefinitionCreator taskDefinitionCreator = new BlockMatrixBasedTaskDefinitionCreator(
                    new DefaultTaskDefinitionBuilderFactory(), firstMatrix.getRowCount(), firstMatrix.getColumnCount(), secondMatrix.getColumnCount(), maxSquareMatrixSize);
            TaskDefinitionRepository taskDefinitionRepository = new InMemoryTaskDefinitionRepository();
            TaskCreator taskCreator = new DefaultTaskCreator(myIp, replyPort);
            TaskSender taskSender = new DefaultTaskSender(new UDPBasedObjectSender<>());
            WorkerBufferHealthChecker healthChecker = new DefaultWorkerBufferHealthChecker(taskDefinitionSendTimeoutInMilliseconds, bufferSuspendTimeoutInMilliseconds);

            System.out.println("Multiplication context has just been started; you can check the log file for process tracking");

            WorkerBuffersManager manager = new DefaultWorkerBuffersManager(new DefaultWorkerBufferProducer(taskDefinitionCreator, taskDefinitionRepository),
                    new DefaultWorkerBufferProcessor(taskCreator, taskSender, healthChecker, firstMatrix, secondMatrix));
            manager.startManagement(workerBuffers, workerBuffersManagementCycleInMilliseconds);

            ResultBuffer resultBuffer = new DefaultResultBuffer();
            DefaultResultReceiver resultReceiver = new DefaultResultReceiver(new UDPBasedObjectReceiver<>(objectReceiverTimeoutInMilliseconds), new DefaultResultBufferProducer(resultBuffer));
            DefaultResultBufferConsumer consumer = new DefaultResultBufferConsumer(taskDefinitionRepository, workerBuffers, resultMatrix, resultBufferConsumeCycleInMilliseconds);
            resultReceiver.receiveTaskResult(replyPort);
            consumer.accept(resultBuffer);

            while (!taskDefinitionCreator.isDonePartitioning()) {
                Thread.sleep(1500);
            }

            LOGGER.log(Level.INFO, "All task definitions are created and not yet finished ones will be splitted to active worker buffers from the ones already suspended");

            RemainingTaskDefinitionProvider remainingTaskDefinitionProvider = new RemainingTaskDefinitionProvider(taskDefinitionRepository, workerBuffers);
            manager = new DefaultWorkerBuffersManager(new DefaultWorkerBufferProducer(remainingTaskDefinitionProvider, taskDefinitionRepository),
                    new DefaultWorkerBufferProcessor(taskCreator, taskSender, healthChecker, firstMatrix, secondMatrix));
            manager.startManagement(workerBuffers, workerBuffersManagementCycleInMilliseconds);


            while (!remainingTaskDefinitionProvider.isDonePartitioning()) {
                Thread.sleep(1500);
            }

            resultBuffer.setFinished();
            resultReceiver.markAsFinished();

            LOGGER.log(Level.INFO, "Matrix multiplication context is about to be finalized after result matrix to file");

            new FilePathBasedMatrixWriter().writeMatrixToTarget(resultMatrix, resultMatrixPath);
            LOGGER.log(Level.INFO, "Finished writing results to " + resultMatrixPath);
            System.out.println("Finished writing results to " + resultMatrixPath);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
