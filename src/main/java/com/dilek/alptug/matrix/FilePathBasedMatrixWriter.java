package com.dilek.alptug.matrix;

import com.dilek.alptug.entity.Matrix;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * A file based {@link MatrixWriter} implementation where target is the path of the file
 * for the matrix to be written.
 *
 * @author alptugd
 */
public class FilePathBasedMatrixWriter implements MatrixWriter<String> {

    @Override
    public void writeMatrixToTarget(Matrix matrix, String target) throws Exception {
        Path path = FileSystems.getDefault().getPath(target);

        List<String> lines = new ArrayList<>();

        IntStream.range(0, matrix.getRowCount()).forEach((i)-> {
            StringBuilder line = new StringBuilder();
            IntStream.range(0, matrix.getColumnCount()).forEach((j)->line.append(matrix.getValue(i, j)).append(FilePathBasedMatrixCreator.SEPARATOR));
            lines.add(line.toString().substring(0, line.lastIndexOf(FilePathBasedMatrixCreator.SEPARATOR)));
        });

        Files.write(path, lines);
    }
}
