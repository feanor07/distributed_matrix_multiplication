package com.dilek.alptug.matrix;

import com.dilek.alptug.entity.Matrix;

/**
 * Functional interface to write a matrix to the specified target.
 *
 * @author alptugd
 */
public interface MatrixWriter<T> {
    void writeMatrixToTarget(Matrix matrix, T target) throws Exception;
}

