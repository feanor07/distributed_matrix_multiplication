package com.dilek.alptug.matrix;

import com.dilek.alptug.entity.InMemoryMatrix;
import com.dilek.alptug.entity.Matrix;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

/**
 * A file based {@link MatrixCreator} implementation where source is the path of the file
 * containing the matrix and the instance to be returned is of type {@link InMemoryMatrix}
 *
 * @author alptugd
 */
public class FilePathBasedMatrixCreator implements MatrixCreator<String> {

    private static final String INVALID_FILE = "InvalidFile";
    static final String SEPARATOR = ",";

    public Matrix createMatrixFromSource(String source) {
        try {
            Path path = FileSystems.getDefault().getPath(source);

            Stream<String> lines = Files.lines(path);
            List<int[]> rows = new ArrayList<>();

            lines.forEach((line)->{
                List<Integer> row = new ArrayList<>();

                for (String s: line.split(SEPARATOR)) {
                    row.add(Integer.parseInt(s));
                }

                rows.add(row.stream().mapToInt(Integer::intValue).toArray());
            });

            int[][] result = new int[rows.size()][rows.get(0).length];

            AtomicInteger i = new AtomicInteger(0);

            rows.stream().forEach((array)->result[i.get()] = rows.get(i.getAndIncrement()));

            return new InMemoryMatrix(result);
        } catch (Exception e) {
            throw new RuntimeException(INVALID_FILE, e);
        }
    }

    public static void main(String[] args) {
        Matrix matrix = new FilePathBasedMatrixCreator().createMatrixFromSource("d:/matrix.txt");
        System.out.println(matrix.getValue(123, 10));
    }
}
