package com.dilek.alptug.matrix;

import com.dilek.alptug.entity.Matrix;

/**
 * Functional interface to create a matrix from the given source
 *
 * @author alptugd
 */
public interface MatrixCreator<T> {
    Matrix createMatrixFromSource(T source);
}
