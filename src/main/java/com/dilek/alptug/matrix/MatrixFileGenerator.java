package com.dilek.alptug.matrix;

import com.dilek.alptug.entity.InMemoryMatrix;

import java.util.stream.IntStream;

/**
 * Helper class to create matrix files for testing purposes
 *
 * @author alptugd
 */
public class MatrixFileGenerator {
    public static void main(String[] args) throws Exception {
        String filePath = args[0];
        int rowCount = Integer.parseInt(args[1]);
        int columnCount = Integer.parseInt(args[2]);
        int minValue = Integer.parseInt(args[3]);
        int maxValue = Integer.parseInt(args[4]);
        InMemoryMatrix matrix = new InMemoryMatrix(new int[rowCount][columnCount]);

        IntStream.range(0, rowCount).forEach((i)->
            IntStream.range(0, columnCount).forEach((j)->{
                int value = minValue + (int) (Math.random() * (maxValue - minValue) + 1);
                matrix.add(i, j, new int[][]{{value}});
            })
        );

        new FilePathBasedMatrixWriter().writeMatrixToTarget(matrix, filePath);
    }
}
