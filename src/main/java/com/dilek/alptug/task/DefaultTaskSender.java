package com.dilek.alptug.task;


import com.dilek.alptug.datasender.ObjectSender;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.Worker;

import java.net.InetSocketAddress;

/**
 * Default {@link TaskSender} implementation. Works as an adapter to {@link ObjectSender} interface.
 *
 * @author alptugd
 */
public class DefaultTaskSender implements TaskSender {
    private final ObjectSender<Task> objectSender;

    public DefaultTaskSender(ObjectSender<Task> objectSender) {
        this.objectSender = objectSender;
    }

    /**
     * Adapter method to send task to the worker via using object sender interface underneath.
     *
     * @param task, Task to offload
     * @param worker, Worker to sent the task to
     *
     * @return whether sending operation succeeded.
     */
    public boolean sendTask(Task task, Worker worker) {
        InetSocketAddress socketAddress = new InetSocketAddress(worker.getIp(), worker.getPort());
        return objectSender.sendObject(task, socketAddress);
    }
}
