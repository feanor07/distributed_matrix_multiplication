package com.dilek.alptug.task;

import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskDefinition;

/**
 * Functional interface enabling creation of tasks from definitions for given matrices
 *
 * @author alptugd
 */
public interface TaskCreator {
    /**
     * Creates and returns a task from given matrices with respect to definition
     *
     * @param taskDefinition, specification of the task to be created
     * @param first, first matrix of the multiplication operation
     * @param second, second matrix of the multiplication operation
     *
     * @return task representing a submultiplication operation
     */
    Task createTask(TaskDefinition taskDefinition, Matrix first, Matrix second);
}

