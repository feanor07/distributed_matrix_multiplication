package com.dilek.alptug.task;

import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.Worker;

/**
 * Functional interface responsible for sending a task to a worker.
 *
 * @author alptugd
 */
public interface TaskSender {
    boolean sendTask(Task task, Worker worker);
}
