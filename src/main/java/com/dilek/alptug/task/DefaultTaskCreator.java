package com.dilek.alptug.task;

import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.Task;
import com.dilek.alptug.entity.TaskDefinition;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Default implementation of {@link TaskCreator} functional interface.
 *
 * @author alptugd
 */
public class DefaultTaskCreator implements TaskCreator {
    private static final Logger LOGGER = Logger.getLogger(DefaultTaskCreator.class.getName());

    private final String replyAddress;
    private final int replyPort;

    public DefaultTaskCreator(String replyAddress, int replyPort) {
        this.replyAddress = replyAddress;
        this.replyPort = replyPort;
    }

    public Task createTask(TaskDefinition taskDefinition, Matrix first, Matrix second) {
        int commonIndex = taskDefinition.getCommonIndex();
        int vectorLength = taskDefinition.getVectorLength();
        int rowCount = taskDefinition.getRowCount();
        int columnCount = taskDefinition.getColumnCount();

        int[][] rows = first.partition(taskDefinition.getResultRowIndex(), commonIndex, rowCount, vectorLength);
        int[][] columns = second.partition(commonIndex, taskDefinition.getResultColumnIndex(), vectorLength, columnCount);

        Task task = new Task();
        task.setRowVectors(flatten(rows));
        task.setColumnVectors(flatten(transpose(columns)));
        int id = taskDefinition.getId();
        task.setTaskId(id);
        task.setVectorLength(vectorLength);
        task.setReplyAddress(replyAddress);
        task.setReplyPort(replyPort);

        LOGGER.log(Level.INFO, "Created task that is to be offloaded to some worker for task definition with id " + id);

        return task;
    }

    private int[] flatten(int[][] matrix) {
        return Stream.of(matrix).flatMapToInt(Arrays::stream).toArray();
    }

    private int[][] transpose(int[][] matrix) {
        int columnCount = matrix[0].length;
        int rowCount = matrix.length;
        int[][] transpose = new int[columnCount][rowCount];

        for(int i = 0; i< columnCount; i++) {
            for(int j=0;j<rowCount;j++) {
                transpose[i][j] = matrix[j][i];
            }
        }

        return transpose;
    }
}
