package com.dilek.alptug.result;

import com.dilek.alptug.entity.Matrix;
import com.dilek.alptug.entity.TaskDefinition;
import com.dilek.alptug.entity.TaskResult;
import com.dilek.alptug.entity.Worker;
import com.dilek.alptug.taskdefinition.TaskDefinitionRepository;
import com.dilek.alptug.worker.WorkerBuffer;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is responsible to consume from result buffer, update task definitions and remove task definitions from
 * worker buffers. It initiates a new thread that works periodically for consumption of task results.
 *
 * @author alptugd
 */
public class DefaultResultBufferConsumer implements Consumer<ResultBuffer> {
    private static final Logger LOGGER = Logger.getLogger(DefaultResultBufferConsumer.class.getName());

    private final TaskDefinitionRepository taskDefinitionRepository;
    private final long consumeCycleInMilliseconds;
    private final Matrix resultMatrix;
    private final Collection<WorkerBuffer> workerBuffers;

    public DefaultResultBufferConsumer(TaskDefinitionRepository taskDefinitionRepository, Collection<WorkerBuffer> workerBuffers,
                                       Matrix resultMatrix, long consumeCycleInMilliseconds) {
        this.taskDefinitionRepository = taskDefinitionRepository;
        this.resultMatrix = resultMatrix;
        this.workerBuffers = workerBuffers;
        this.consumeCycleInMilliseconds = consumeCycleInMilliseconds;
    }

    @Override
    public void accept(ResultBuffer buffer) {
        initiateNewThreadForConsumption(buffer);
    }

    /**
     * Retrieves buffered results from the buffer, marks the relevant task definitions as finished, removes completed task
     * definitions from each worker buffer.
     *
     * @param buffer, buffer to consume
     */
    private void initiateNewThreadForConsumption(ResultBuffer buffer) {
        new Thread(()->{
            while (!buffer.isDone()) {
                Collection<TaskResult> taskResults = buffer.retrieveTaskResults();

                taskResults.stream().forEach((taskResult -> {
                    int id = taskResult.getTaskId();
                    boolean isTaskDefinitionNewlyFinished = taskDefinitionRepository.markTaskDefinitionAsFinished(id);
                    addToResultMatrix(taskResult, id, isTaskDefinitionNewlyFinished);
                    updateWorkerBufferResponseTime(taskResult.getReceivedFromIp(), taskResult.getReceivedFromPort());

                    try {
                        Thread.sleep(consumeCycleInMilliseconds);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }));
            }
        }).start();
    }

    private void updateWorkerBufferResponseTime(String receivedFromIp, int receivedFromPort) {
        Optional<WorkerBuffer> receivedForBuffer = workerBuffers.stream().filter((workerBuffer)->{
            Worker worker = workerBuffer.getWorker();
            return receivedFromIp.equals(worker.getIp()) && receivedFromPort == worker.getPort();
        }).findFirst();

        LOGGER.log(Level.INFO, "Last response time of worker with ip " + receivedFromIp + " and " + receivedFromPort + " is about to be updated");
        receivedForBuffer.ifPresent((buffer -> buffer.setLastResponseTime(System.currentTimeMillis())));
    }


    private void addToResultMatrix(TaskResult taskResult, int id, boolean isTaskDefinitionNewlyFinished) {
        if (isTaskDefinitionNewlyFinished) {
            LOGGER.log(Level.INFO, "Adding retrieved task result for task definition with id " + id + " to result matrix");
            TaskDefinition taskDefinition = taskDefinitionRepository.getTaskDefinition(id);
            resultMatrix.add(taskDefinition.getResultRowIndex(), taskDefinition.getResultColumnIndex(), taskResult.getResult());
        } else {
            LOGGER.log(Level.INFO, "Retrieved duplicate task result for task definition with id " + id + " hence no addition to result matrix will be performed");
        }
    }
}
