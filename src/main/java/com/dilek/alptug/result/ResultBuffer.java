package com.dilek.alptug.result;

import com.dilek.alptug.entity.TaskResult;

import java.util.Collection;

/**
 * Interface representing result buffer holding task results retrieved from workers.
 *
 * @author alptugd
 */
public interface ResultBuffer {
    /**
     * Puts the given task result to the buffer.
     *
     * @param taskResult, task result to buffer
     */
    void putTaskResult(TaskResult taskResult);

    /**
     * Removes and returns all available task results in the buffer.
     * @return all available task results upon removal
     */
    Collection<TaskResult> retrieveTaskResults();

    /**
     * Marks the buffer as finished to indicate there are no more task results to come.
     */
    void setFinished();

    /**
     * Returns if the result buffer is done and there will be no more results to consume.
     *
     * @return whether the buffer is marked as done
     */
    boolean isDone();
}
