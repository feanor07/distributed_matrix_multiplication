package com.dilek.alptug.result;

import com.dilek.alptug.entity.TaskResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Default {@link ResultBuffer} implementation that is inherently synchronized in terms of puting to and removing from
 * list.
 *
 * @author alptugd
 */
public class DefaultResultBuffer implements ResultBuffer {

    private boolean isFinished = false;

    private List<TaskResult> taskResults;

    public DefaultResultBuffer() {
        taskResults = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public void putTaskResult(TaskResult taskResult) {
        if (this.isFinished) {
            throw new IllegalStateException("Cannot accept new result as the buffer is marked finished");
        }

        taskResults.add(taskResult);
    }

    @Override
    public Collection<TaskResult> retrieveTaskResults() {
        List<TaskResult> result = new ArrayList<>();

        while (!taskResults.isEmpty()) {
            TaskResult taskResult = taskResults.remove(0);
            result.add(taskResult);
        }

        return result;
    }

    @Override
    public void setFinished() {
        this.isFinished = true;
    }

    @Override
    public boolean isDone() {
        return taskResults.isEmpty() && this.isFinished;
    }
}
