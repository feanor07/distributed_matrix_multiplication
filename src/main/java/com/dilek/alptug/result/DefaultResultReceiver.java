package com.dilek.alptug.result;

import com.dilek.alptug.datareceiver.ObjectReceiver;
import com.dilek.alptug.entity.TaskResult;

import java.net.SocketTimeoutException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default {@link ResultReceiver} implementation. Works as an adapter to {@link ObjectReceiver} interface.
 * Starts the object receiver in a separate thread.
 *
 * @author alptugd
 */
public class DefaultResultReceiver implements  ResultReceiver {
    private static final Logger LOGGER = Logger.getLogger(DefaultResultReceiver.class.getName());

    private final ObjectReceiver<TaskResult> objectReceiver;
    private boolean isFinished = false;
    private final Consumer<TaskResult> consumer;

    public DefaultResultReceiver(ObjectReceiver<TaskResult> objectReceiver, Consumer<TaskResult> consumer) {
        this.objectReceiver = objectReceiver;
        this.consumer = consumer;
    }

    @Override
    public void receiveTaskResult(int port) {
        new Thread(()-> {
            try {
                LOGGER.log(Level.INFO, "Task results will be gathered from " + port + " with UDP protocol");
                initiateTaskReceiving(port);
            } catch (Exception e) {
                if (e.getCause() instanceof SocketTimeoutException) {
                    if (!isFinished) {
                        LOGGER.log(Level.INFO, "Restarting udp task receiver that had timed out since matrix multiplication context is not finalized yet");
                        receiveTaskResult(port);
                    }
                } else {
                    throw e;
                }
            }
        }).start();
    }

    private void initiateTaskReceiving(int port) {
        objectReceiver.startReceiving(port, consumer);
    }

    @Override
    public void markAsFinished() {
        this.isFinished = true;
    }
}
