package com.dilek.alptug.result;

/**
 * Interface responsible for receiving a task result from a specified port as long as not finished.
 *
 * @author alptugd
 */
public interface ResultReceiver {
    /**
     * Receives task result from the given port
     * @param port to receive task result
     */
    void receiveTaskResult(int port);

    /**
     * Marks the receiver as finished
     */
    void markAsFinished();
}
