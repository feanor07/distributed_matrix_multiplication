package com.dilek.alptug.result;

import com.dilek.alptug.entity.TaskResult;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Responsible for producing task results for result buffer.
 *
 * @author alptugd
 */
public class DefaultResultBufferProducer implements Consumer<TaskResult> {
    private static final Logger LOGGER = Logger.getLogger(DefaultResultBufferProducer.class.getName());

    private final ResultBuffer buffer;

    public DefaultResultBufferProducer(ResultBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void accept(TaskResult taskResult) {
        buffer.putTaskResult(taskResult);
        LOGGER.log(Level.INFO, "Received task result for task definition with id " + taskResult.getTaskId() + " and put it to result buffer");
    }
}
