package com.dilek.alptug;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.*;

/**
 * Main application class that takes port number (for receiving task results), file path of the matrices and path of
 * file containing hosts from the user through console and starts a multiplication context. Note that concurrent multiplication
 * contexts can be created.
 *
 * @author alptugd
 */

class ContextAbortedException extends RuntimeException {
}

public class Application {

    /**private static final int REPLY_PORT = 1234;
    private static final String FIRST_MATRIX_PATH = "d:/firstMatrix.txt";
    private static final String SECOND_MATRIX_PATH = "d:/secondMatrix.txt";
    private static final String RESULT_MATRIX_PATH = "d:/resultMatrix.txt";
    private static final String HOSTS_FILE_PATH = "d:/hosts.txt";*/
    // Worker buffer capacity for task definitions
    private static final int WORKER_BUFFER_CAPACITY = 10;
    // Max square matrix siz that should fit into UDP datagram
    private static final int MAX_SQUARE_MATRIX_SIZE = 90;
    // Sleep time for Worker buffers management thread
    // (the thread that performs health check, removes task definitions, creates child threads to create and send tasks)
    private static final int WORKER_BUFFERS_MANAGEMENT_CYCLE_IN_MILLISECONDS = 500;
    // Sleep time for the thread that is responsible to consume the result buffer
    private static final int RESULT_BUFFER_CONSUME_CYCLE_IN_MILLISECONDS = 10;
    // The time out time for a task definition to be marked for resending if no result arrives after sent
    private static final int TASK_DEFINITION_SEND_TIMEOUT_IN_MILLISECONDS = 20000;
    // The time out time for worker buffer to be marked as suspended.
    // It is the time that elapses from the time last result retrived from the worker of this buffer
    private static final int BUFFER_SUSPEND_TIMEOUT_IN_MILLISECONDS = 60000;
    // The UDP server socket time out
    private static final int OBJECT_RECEIVER_TIMEOUT_IN_MILLISECONDS = 10000;

    public static void main(String[] args) throws Exception {
        setLoggingParameters();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please press N and hit enter to start new multiplication context");
        System.out.println("You can press A and hit enter to abort starting a new context");
        System.out.println("You can press X and hit enter to kill the application");

        while (true) {
            String s = scanner.next();

            try {
                if (s.equals("N")) {
                    System.out.println("Starting new multiplication context");
                    startNewMultiplicationContext(scanner);
                }

                checkIfContextAborted(s);
                checkIfAppKilled(s);
            } catch (ContextAbortedException e) {
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
     }

     private static  void checkIfContextAborted(String s) {
        boolean result = s.equals("A");

        if (result) {
            System.out.println("Aborted current multiplication context. Start N to intiate a new one");
            throw new ContextAbortedException();
        }
     }

     private static void checkIfAppKilled(String s) {
        if (s.equals("X")) {
            System.exit(0);
        }
     }

     private static void startNewMultiplicationContext(Scanner scanner) {
         System.out.println("Please enter unique port number");
         int port = getInt(scanner);
         System.out.println("Please enter the path of the file containing first matrix");
         String firstMatrixPath =  getString(scanner);
         System.out.println("Please enter the path of the file containing second matrix");
         String secondMatrixPath = getString(scanner);
         System.out.println("Please enter the path of host file containing worker information");
         String hostsFilePath = getString(scanner);
         System.out.println("Please enter the path of the result file to be created");
         String resultMatrixPath = getString(scanner);
         raiseNewContextInSeperateThread(port, firstMatrixPath, secondMatrixPath, hostsFilePath, resultMatrixPath);
     }

     private static String getString(Scanner scanner) {
         String s = scanner.next();

         checkIfContextAborted(s);
         checkIfAppKilled(s);

         return s;
     }

     private static int getInt(Scanner scanner) {
        return scanner.nextInt();
     }

    private static void setLoggingParameters() throws IOException {
        // get the global logger to configure it
        Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

        // suppress the logging output to the console
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            rootLogger.removeHandler(handlers[0]);
        }

        logger.setLevel(Level.INFO);
        Handler handler = new FileHandler("application.log", 10000000, 3);
        Logger.getLogger("").addHandler(handler);
    }

    private static void raiseNewContextInSeperateThread(int replyPort, String firstMatrixPath, String secondMatrixPath, String hostsFilePath, String resultMatrixPath) {
            new Thread(new MultiplicationContext(firstMatrixPath, secondMatrixPath, hostsFilePath, resultMatrixPath,
                    WORKER_BUFFER_CAPACITY, replyPort, MAX_SQUARE_MATRIX_SIZE,
                    WORKER_BUFFERS_MANAGEMENT_CYCLE_IN_MILLISECONDS, RESULT_BUFFER_CONSUME_CYCLE_IN_MILLISECONDS,
                    TASK_DEFINITION_SEND_TIMEOUT_IN_MILLISECONDS, BUFFER_SUSPEND_TIMEOUT_IN_MILLISECONDS,
                    OBJECT_RECEIVER_TIMEOUT_IN_MILLISECONDS)).start();
     }
}
