# README #

This README simply explains the Distributed_Matrix_Multiplication project that is written with Pure Jdk 8.

### What is this repository for? ###

* This is the main part (task distributor and result collector) of distributed matrix multiplication assignment. This class takes parameters from the console so as to start a new matrix multiplication session. It reads matrices to multiply from the files. Partitions the task with respect to the [Block Matrix Partitioning](https://en.wikipedia.org/wiki/Block_matrix). Partitioning is performed as meta data (named task definition in the source code; that is sub partitions to send to workers are not created until actual send operation to workers is to be performed) and each individual worker is fed with unique task definitions until partitioning phase is over. Timed out task definitions are sent to the same worker over and over again. A worker is expected to answer at some point possibly rerunning after a failure or intermittent network connection. After block matrix partitioning is over; workers from which no answer is retrieved for a long time are iterated and their workload is tried to be distributed among active workers evenly. After results of all task definitions are collected; the final result is written to the file for which the path had already been provided by the user before initiating the matrix multiplication session.

* Version: 1.0-SNAPSHOT

### How do I get set up? ###

* Checkout the source code and just execute the Application main class. You will be guided appropriately to initiate a matrix multiplication session. Note that you do not need to wait for a session to finish before starting another session.
* You can also run the unit tests.
* The project has no dependencies other than Mockito and Junit libraries; which are only used for testing purposes.
* The repository also contains an executable jar; "distributed_matrix_multiplication.jar". You can run the jar via "java -jar distributed_matrix_multiplication.jar" command with jre 8.

### Contribution guidelines ###

* No one is expected to contribute to this repository other than owner.

### Who do I talk to? ###

* Repo owner.